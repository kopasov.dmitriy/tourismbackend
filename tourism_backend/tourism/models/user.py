from ..models.region import Region
from .user_uploaded_file import UserUploadedFile
from django.db import models


class User(models.Model):
    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'
        ordering = ['surname', 'first_name']

    email = models.EmailField(max_length=100, verbose_name='Адрес эл. почты')
    first_name = models.CharField(max_length=200, verbose_name='Имя')
    surname = models.CharField(max_length=200, verbose_name='Фамилия')
    additional_name = models.CharField(max_length=200, verbose_name='Отчество (доп. имя)', blank=True, null=True)
    country = models.CharField(max_length=200, verbose_name='Страна', blank=True, null=True)
    region = models.ForeignKey(Region, on_delete=models.SET_NULL, verbose_name='Область',
                               blank=True, null=True)
    city = models.CharField(max_length=200, verbose_name='Город', blank=True, null=True)
    image_file = models.ForeignKey(UserUploadedFile,
                                   on_delete=models.SET_NULL,
                                   verbose_name='Картинка',
                                   related_name='profile_images',
                                   blank=True,
                                   null=True)
    phone_number = models.CharField(max_length=20, verbose_name='Номер телефона', blank=True, null=True)
    date_of_birthday = models.DateField(verbose_name='Дата рождения', blank=True, null=True)
    verified = models.BooleanField(default=False, verbose_name='Пользователь подтверждён?')

    created = models.DateTimeField(auto_now_add=True, verbose_name='Создано')
    modified = models.DateTimeField(auto_now=True, verbose_name='Изменено')

    def __str__(self):
        return f'{self.first_name} {self.surname} ({self.email})'
