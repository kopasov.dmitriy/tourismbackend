from .route import Route
from django.db import models


class RatingRoute(models.Model):
    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['route'], name='rating_route'),
        ]
        verbose_name = 'Рейтинг маршрута'
        verbose_name_plural = 'Рейтинги маршрутов'
        ordering = ['route', 'avg_rating']

    route = models.ForeignKey(Route, on_delete=models.CASCADE, verbose_name='Маршрут')

    avg_rating = models.FloatField(default=0, verbose_name='Общая средняя оценка')
    avg_rating_org = models.FloatField(verbose_name='Организация')
    avg_rating_imp = models.FloatField(verbose_name='Впечатления')
    avg_rating_service = models.FloatField(verbose_name='Сервис')
    avg_rating_price = models.FloatField(verbose_name='Цена')

    number_of_ratings = models.IntegerField(default=0, verbose_name='Количество оценок')

    def __str__(self):
        rating = '' if self.avg_rating is None or self.avg_rating == 0 else f' (рейтинг - {self.avg_rating})'
        return f'{self.route}{rating}'
