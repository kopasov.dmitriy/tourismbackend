from .user import User
from django.db import models


class UserToken(models.Model):
    class Meta:
        verbose_name = 'Токен доступа пользователя'
        verbose_name_plural = 'Токены доступа пользователей'
        ordering = ['user__surname', 'user__first_name', '-last_active', 'device_name']

    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='Пользователь')
    token = models.CharField(max_length=64, verbose_name='Токен доступа')
    device_name = models.CharField(max_length=200, verbose_name='Устройство')
    last_active = models.DateTimeField(verbose_name='Последняя активность')

    def __str__(self):
        return f'{self.user} - {self.token}'
