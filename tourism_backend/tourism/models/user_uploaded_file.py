from django.db import models


class UserUploadedFile(models.Model):
    class Meta:
        verbose_name = 'Файл'
        verbose_name_plural = 'Файлы'
        ordering = ['file_id']

    file_id = models.CharField(max_length=100, verbose_name='Идентификатор файла')
    small_file_id = models.CharField(max_length=100, verbose_name='Идентификатор сжатого файла (для фото)',
                                     blank=True, null=True)
    type = models.CharField(max_length=50, verbose_name='Тип файла')

    created = models.DateTimeField(auto_now_add=True, verbose_name='Создано')
    created_by = models.CharField(max_length=100, verbose_name='Кто добавил?')

    def __str__(self):
        return f'{self.created_by} - {self.file_id}'
