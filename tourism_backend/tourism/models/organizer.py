from .user import User
from .user_uploaded_file import UserUploadedFile
from django.db import models


class Organizer(models.Model):
    class Meta:
        verbose_name = 'Организатор'
        verbose_name_plural = 'Организаторы'
        ordering = ['name']

    name = models.CharField(max_length=200, verbose_name='Название')
    user = models.OneToOneField(User, on_delete=models.SET_NULL, verbose_name='Пользователь', blank=True, null=True)

    passport_series_enc = models.CharField(max_length=200, verbose_name='Серия паспотра (зашифр)')
    passport_number_enc = models.CharField(max_length=200, verbose_name='Номер паспотра (зашифр)')
    passport_series_number_hash = models.CharField(max_length=200, verbose_name='Серия и номер паспотра (хэш)')
    passport_issue_date_enc = models.CharField(max_length=200, verbose_name='Дата выдачи (зашифр)')
    passport_issue_authority_enc = models.CharField(max_length=1000, verbose_name='Орган выдачи (зашифр)')
    passport_original_file = models.ForeignKey(UserUploadedFile,
                                               on_delete=models.SET_NULL,
                                               verbose_name='Фото паспорта',
                                               related_name='passport_original_files',
                                               blank=True,
                                               null=True)
    individual_tax_number_enc = models.CharField(max_length=200, verbose_name='ИНН (зашифр)')
    individual_tax_number_hash = models.CharField(max_length=200, verbose_name='ИНН (хэш)')
    individual_tax_number_original_file = models.ForeignKey(UserUploadedFile,
                                                            on_delete=models.SET_NULL,
                                                            verbose_name='Фото ИНН',
                                                            related_name='individual_tax_number_original_files',
                                                            blank=True,
                                                            null=True)

    verified = models.BooleanField(default=False, verbose_name='Организация подтверждена?')

    created = models.DateTimeField(auto_now_add=True, verbose_name='Создано')
    modified = models.DateTimeField(auto_now=True, verbose_name='Изменено')

    def __str__(self):
        return self.name
