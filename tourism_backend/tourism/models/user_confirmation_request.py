from .user import User
from django.db import models


class UserConfirmationRequest(models.Model):
    class Meta:
        verbose_name = 'Пользовательский запрос на подтверждение'
        verbose_name_plural = 'Пользовательские запросы на подтверждение'
        ordering = ['user__surname']

    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='Пользователь')
    key = models.CharField(max_length=60, verbose_name='Ключ подтверждения')
    type = models.CharField(max_length=10, verbose_name='Тип подверждения (email/password)')

    created = models.DateTimeField(auto_now_add=True, verbose_name='Создано')

    def __str__(self):
        return f'{self.user} - {self.type}'
