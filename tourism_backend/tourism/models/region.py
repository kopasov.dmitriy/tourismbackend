from django.db import models


class Region(models.Model):
    class Meta:
        verbose_name = 'Регион'
        verbose_name_plural = 'Регионы'
        ordering = ['name']

    name = models.CharField(max_length=150, verbose_name='Наименование региона')
    image = models.CharField(max_length=100, verbose_name='Идентификатор картинки региона')
    latitude = models.FloatField(default=0, verbose_name='Широта')
    longitude = models.FloatField(default=0, verbose_name='Долгота')

    def __str__(self):
        return self.name
