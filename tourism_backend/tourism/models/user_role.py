from .role import Role
from .user import User
from django.db import models


class UserRole(models.Model):
    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['user', 'role'], name='user_role'),
        ]
        verbose_name = 'Роль пользователя'
        verbose_name_plural = 'Роли пользователей'

    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='Пользователь')
    role = models.ForeignKey(Role, on_delete=models.CASCADE, verbose_name='Роль')

    def __str__(self):
        return f'{self.user} ({self.role})'
