from .difficulty_type import DifficultyType
from .organizer import Organizer
from .region import Region
from .target_group import TargetGroup
from .user_uploaded_file import UserUploadedFile
from django.db import models


# Image access - http://drive.google.com/uc?export=view&id=123
# Root folder id - 17UcQA1cwH0ezNKH05PT8kyhQCkdbjWnB
# Upload guide - https://www.dezyre.com/recipes/upload-files-to-google-drive-using-python
# Credentials problem - https://coderoad.ru/24419188/%D0%90%D0%B2%D1%82%D0%BE%D0%BC%D0%B0%D1%82%D0%B8%D0%B7%D0%B0%D1%86%D0%B8%D1%8F-%D0%BF%D1%80%D0%BE%D1%86%D0%B5%D1%81%D1%81%D0%B0-%D0%B2%D0%B5%D1%80%D0%B8%D1%84%D0%B8%D0%BA%D0%B0%D1%86%D0%B8%D0%B8-pydrive
class Route(models.Model):
    class Meta:
        verbose_name = 'Маршрут'
        verbose_name_plural = 'Маршруты'
        ordering = ['name']

    name = models.CharField(max_length=200, verbose_name='Название')
    description = models.TextField(max_length=1000, verbose_name='Описание')
    location_latitude = models.FloatField(default=0, verbose_name='Широта', blank=True, null=True)
    location_longitude = models.FloatField(default=0, verbose_name='Долгота', blank=True, null=True)
    country = models.CharField(max_length=200, verbose_name='Страна', blank=True, null=True)
    region = models.ForeignKey(Region, on_delete=models.SET_NULL, verbose_name='Область', blank=True, null=True)
    city = models.CharField(max_length=200, verbose_name='Город', blank=True, null=True)
    cost = models.FloatField(default=0, verbose_name='Стоимость')
    image_file = models.ForeignKey(UserUploadedFile,
                                   on_delete=models.SET_NULL,
                                   verbose_name='Картинка',
                                   related_name='route_images',
                                   blank=True,
                                   null=True)
    distance = models.IntegerField(default=0, verbose_name='Протяжённость', blank=True, null=True)

    min_tourists = models.PositiveSmallIntegerField(default=1, verbose_name='Мин туристов')
    max_tourists = models.PositiveSmallIntegerField(default=1, verbose_name='Макс туристов', blank=True, null=True)

    organizer = models.ForeignKey(Organizer, on_delete=models.SET_NULL,
                                  verbose_name='Организатор', blank=True, null=True)

    target_group = models.ForeignKey(TargetGroup, on_delete=models.SET_NULL,
                                     verbose_name='Целевая группа', blank=True, null=True)
    difficulty_type = models.ForeignKey(DifficultyType, on_delete=models.SET_NULL,
                                        verbose_name='Вид сложности', blank=True, null=True)

    start_date = models.DateField(verbose_name='Дата начала', blank=True, null=True)
    end_date = models.DateField(verbose_name='Дата окончания', blank=True, null=True)

    created = models.DateTimeField(auto_now_add=True, verbose_name='Создано')
    created_by = models.CharField(max_length=100, verbose_name='Кто добавил?')
    modified = models.DateTimeField(auto_now=True, verbose_name='Изменено')

    def __str__(self):
        if float(str(self.cost)) > 0:
            return f'{self.name} (стоимость - {self.cost})'
        else:
            return self.name
