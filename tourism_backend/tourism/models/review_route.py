from .route import Route
from .user import User
from django.db import models


class ReviewRoute(models.Model):
    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['route', 'user'], name='review_route'),
        ]
        verbose_name = 'Обзор маршрута'
        verbose_name_plural = 'Обзоры маршрутов'
        ordering = ['user', 'route', 'rating']

    user = models.ForeignKey(User, on_delete=models.SET_NULL, verbose_name='Пользователь', blank=True, null=True)
    route = models.ForeignKey(Route, on_delete=models.CASCADE, verbose_name='Маршрут')

    rating = models.FloatField(verbose_name='Общая оценка')
    rating_org = models.FloatField(verbose_name='Организация')
    rating_imp = models.FloatField(verbose_name='Впечатления')
    rating_service = models.FloatField(verbose_name='Сервис')
    rating_price = models.FloatField(verbose_name='Цена')

    review_text = models.TextField(max_length=1000, verbose_name='Текст', blank=True, null=True)

    created = models.DateTimeField(auto_now_add=True, verbose_name='Создано')
    modified = models.DateTimeField(auto_now=True, verbose_name='Изменено')

    def __str__(self):
        user = self.user if self.user is not None else 'NULL'
        return f'{self.route}: Оценка {self.rating} от {user}'
