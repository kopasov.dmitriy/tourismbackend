from django.db import models


class Role(models.Model):
    class Meta:
        verbose_name = 'Роль'
        verbose_name_plural = 'Роли'
        ordering = ['id']

    name = models.CharField(max_length=100, verbose_name='Название роли')
    can_add_routes = models.BooleanField(default=False, verbose_name='Может добавлять маршруты')
    can_add_places = models.BooleanField(default=False, verbose_name='Может добавлять места')
    can_view_confidential_data = models.BooleanField(default=False, verbose_name='Может видеть серкретную информацию')
    can_admin = models.BooleanField(default=False, verbose_name='Администрирование')
    description = models.TextField(max_length=1000, verbose_name='Описание роли')
    image = models.CharField(max_length=100, verbose_name='Идентификатор картинки')

    def __str__(self):
        return self.name
