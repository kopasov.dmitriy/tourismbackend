from .region import Region
from .user_uploaded_file import UserUploadedFile
from django.db import models


class Place(models.Model):
    class Meta:
        verbose_name = 'Место'
        verbose_name_plural = 'Места'
        ordering = ['name']

    name = models.CharField(max_length=200, verbose_name='Название')
    short_description = models.TextField(max_length=1000, verbose_name='Краткое описание')
    description = models.TextField(max_length=10000, verbose_name='Описание')
    location_latitude = models.FloatField(default=0, verbose_name='Широта', blank=True, null=True)
    location_longitude = models.FloatField(default=0, verbose_name='Долгота', blank=True, null=True)
    country = models.CharField(max_length=200, verbose_name='Страна', blank=True, null=True)
    region = models.ForeignKey(Region, on_delete=models.SET_NULL, verbose_name='Область', blank=True, null=True)
    city = models.CharField(max_length=200, verbose_name='Город', blank=True, null=True)
    image_file = models.ForeignKey(UserUploadedFile,
                                   on_delete=models.SET_NULL,
                                   verbose_name='Картинка',
                                   related_name='place_images',
                                   blank=True,
                                   null=True)

    created = models.DateTimeField(auto_now_add=True, verbose_name='Создано')
    created_by = models.CharField(max_length=100, verbose_name='Кто добавил?')
    modified = models.DateTimeField(auto_now=True, verbose_name='Изменено')

    def __str__(self):
        return self.name
