from django.db import models


class TourismType(models.Model):
    class Meta:
        verbose_name = 'Вид туризма'
        verbose_name_plural = 'Виды туризма'
        ordering = ['name']

    name = models.CharField(max_length=200, verbose_name='Название', unique=True)
    description = models.CharField(max_length=1000, verbose_name='Описание', blank=True, null=True)

    def __str__(self):
        return self.name
