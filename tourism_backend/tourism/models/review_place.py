from .place import Place
from .user import User
from django.db import models


class ReviewPlace(models.Model):
    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['place', 'user'], name='review_place'),
        ]
        verbose_name = 'Обзор места'
        verbose_name_plural = 'Обзоры мест'
        ordering = ['user', 'place', 'rating']

    user = models.ForeignKey(User, on_delete=models.SET_NULL, verbose_name='Пользователь', blank=True, null=True)
    place = models.ForeignKey(Place, on_delete=models.CASCADE, verbose_name='Место')

    rating = models.FloatField(verbose_name='Общая оценка')
    rating_org = models.FloatField(verbose_name='Организация')
    rating_imp = models.FloatField(verbose_name='Впечатления')
    rating_service = models.FloatField(verbose_name='Сервис')
    rating_price = models.FloatField(verbose_name='Цена')

    review_text = models.TextField(max_length=1000, verbose_name='Текст', blank=True, null=True)

    created = models.DateTimeField(auto_now_add=True, verbose_name='Создано')
    modified = models.DateTimeField(auto_now=True, verbose_name='Изменено')

    def __str__(self):
        user = self.user if self.user is not None else 'NULL'
        return f'{self.place}: Оценка {self.rating} от {user}'
