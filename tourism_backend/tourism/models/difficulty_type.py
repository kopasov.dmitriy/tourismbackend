from django.db import models


class DifficultyType(models.Model):
    class Meta:
        verbose_name = 'Вид сложности'
        verbose_name_plural = 'Виды сложности'
        ordering = ['order_priority']

    name = models.CharField(max_length=200, verbose_name='Название', unique=True)
    description = models.CharField(max_length=1000, verbose_name='Описание', blank=True, null=True)
    order_priority = models.IntegerField(default=1, verbose_name='Порядок сортировки')

    def __str__(self):
        return self.name
