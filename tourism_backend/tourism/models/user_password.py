from .user import User
from django.db import models


class UserPassword(models.Model):
    class Meta:
        verbose_name = 'Пароль пользователя'
        verbose_name_plural = 'Пароли пользователей'
        ordering = ['user__surname']

    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='Пользователь')
    password_hash = models.CharField(max_length=32, verbose_name='Хэш пароля')

    created = models.DateTimeField(auto_now_add=True, verbose_name='Создано')
    modified = models.DateTimeField(auto_now=True, verbose_name='Изменено')

    def __str__(self):
        return f'{self.user} - {self.password_hash}'
