from .tourism_type import TourismType
from .route import Route
from django.db import models


class RouteTourismType(models.Model):
    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['route', 'tourism_type'], name='route_tourism_type'),
        ]
        verbose_name = 'Вид туризма маршрута'
        verbose_name_plural = 'Виды туризма маршрутов'

    route = models.ForeignKey(Route, on_delete=models.CASCADE, verbose_name='Маршрут')
    tourism_type = models.ForeignKey(TourismType, on_delete=models.CASCADE, verbose_name='Вид туризма')

    def __str__(self):
        return f'{str(self.route)} - {str(self.tourism_type)}'
