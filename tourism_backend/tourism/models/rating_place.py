from .place import Place
from django.db import models


class RatingPlace(models.Model):
    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['place'], name='rating_place'),
        ]
        verbose_name = 'Рейтинг места'
        verbose_name_plural = 'Рейтинги мест'
        ordering = ['place', 'avg_rating']

    place = models.ForeignKey(Place, on_delete=models.CASCADE, verbose_name='Место')

    avg_rating = models.FloatField(default=0, verbose_name='Общая средняя оценка')
    avg_rating_org = models.FloatField(verbose_name='Организация')
    avg_rating_imp = models.FloatField(verbose_name='Впечатления')
    avg_rating_service = models.FloatField(verbose_name='Сервис')
    avg_rating_price = models.FloatField(verbose_name='Цена')

    number_of_ratings = models.IntegerField(default=0, verbose_name='Количество оценок')

    def __str__(self):
        rating = '' if self.avg_rating is None or self.avg_rating == 0 else f' (рейтинг - {self.avg_rating})'
        return f'{self.place}{rating}'
