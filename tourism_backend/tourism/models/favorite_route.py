from .route import Route
from .user import User
from django.db import models


class FavoriteRoute(models.Model):
    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['route', 'user'], name='route_user'),
        ]
        verbose_name = 'Избранный маршрут'
        verbose_name_plural = 'Избранные маршруты'
        ordering = ['route__name', 'user__id']

    route = models.ForeignKey(Route, on_delete=models.CASCADE, verbose_name='Маршрут')
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='Пользователь')

    created = models.DateTimeField(auto_now_add=True, verbose_name='Создано')

    def __str__(self):
        return f'{self.route} - {self.user}'
