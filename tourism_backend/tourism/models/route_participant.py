from .route import Route
from .user import User
from django.db import models


class RouteParticipant(models.Model):
    class Meta:
        verbose_name = 'Участник маршрута'
        verbose_name_plural = 'Участники маршрута'
        ordering = ['route__name']
        constraints = [
            models.UniqueConstraint(fields=['route', 'user'], name='route_user_participant'),
        ]

    route = models.ForeignKey(Route, on_delete=models.CASCADE, verbose_name='Маршрут')
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='Пользователь')
    reserved_places = models.PositiveSmallIntegerField(default=1, verbose_name='Мест зарезервировано')
    message = models.TextField(max_length=1000, verbose_name='Сообщение')
    payment = models.FloatField(default=0, verbose_name='Оплачено')

    confirmed = models.BooleanField(default=False, verbose_name='Участие подтверждено?')

    def __str__(self):
        return f'{self.route.name} - {str(self.user)}'
