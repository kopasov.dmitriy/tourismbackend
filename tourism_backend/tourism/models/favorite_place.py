from .place import Place
from .user import User
from django.db import models


class FavoritePlace(models.Model):
    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['place', 'user'], name='place_user'),
        ]
        verbose_name = 'Избранное место'
        verbose_name_plural = 'Избранные места'
        ordering = ['place__name', 'user__id']

    place = models.ForeignKey(Place, on_delete=models.CASCADE, verbose_name='Место')
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='Пользователь')

    created = models.DateTimeField(auto_now_add=True, verbose_name='Создано')

    def __str__(self):
        return f'{self.place} - {self.user}'
