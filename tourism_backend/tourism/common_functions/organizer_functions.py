from ..exceptions.exceptions import CommonException
from ..models.organizer import Organizer
from ..models.user import User
from .auth_functions import get_current_user


def get_current_organizer(request_or_user, return_none=False):
    user = get_current_user(request_or_user, update_last_active=True) if type(request_or_user) != User \
        else request_or_user
    organizer = Organizer.objects.filter(user=user).first()
    if return_none:
        return organizer
    if organizer is None:
        raise CommonException('Пользователь не является организатором')
    return organizer


def get_current_organizer_or_none(request_or_user):
    return get_current_organizer(request_or_user, return_none=True)
