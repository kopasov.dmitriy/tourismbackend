from ..exceptions.exceptions import CommonException
from ..models.place import Place


def get_place_by_id(place_id):
    if place_id is None:
        raise CommonException('Не указан идентификатор места')
    return Place.objects.filter(id=place_id).first()


def get_existing_place_by_id(place_id):
    place = get_place_by_id(place_id)
    if place is None:
        raise CommonException(f'Место с id {place_id} не найдено')
    return place
