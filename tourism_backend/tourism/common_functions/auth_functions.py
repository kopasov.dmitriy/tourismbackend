from ..exceptions.exceptions import AuthException, CommonException, EmailConfirmationRequiredException
from ..models.role import Role
from ..models.user import User
from ..models.user_password import UserPassword
from ..models.user_role import UserRole
from ..models.user_token import UserToken
from django.utils.timezone import now
from secrets import token_urlsafe
import hashlib
import json
import requests


def create_user(email, phone_number, password,
                first_name, surname, additional_name,
                country, region, city, date_of_birthday):
    user = User(email=email,
                phone_number=phone_number,
                first_name=first_name,
                surname=surname,
                additional_name=additional_name,
                country=country,
                region=region,
                city=city,
                date_of_birthday=date_of_birthday)

    role_tourist = Role.objects.filter(name='Турист').first()
    user_role_tourist = UserRole(role=role_tourist, user=user)

    user.save()
    role_tourist.save()
    user_role_tourist.save()

    if password is not None:
        user_password = UserPassword(user=user, password_hash=hash_password(password))
        user_password.save()

    return user


def auth(user, device_name):
    user_token = UserToken(user=user, token=generate_auth_token(64), device_name=device_name, last_active=now())
    user_token.save()
    return user_token


def get_google_user_info(google_token):
    if google_token is None or len(google_token) == 0:
        raise CommonException('Не указан ключ авторизации Google')
    response = requests.get(f'https://www.googleapis.com/oauth2/v3/tokeninfo?id_token={google_token}')
    if response is None or response.status_code != 200:
        raise CommonException('Указан недействительный ключ авторизации Google')
    return json.loads(response.content)


def get_current_user_token(request, update_last_active=False):
    if 'token' not in request.headers:
        raise AuthException('Пользователь не авторизован')
    token = request.headers['token']
    db_token = UserToken.objects.filter(token=token)
    if len(db_token) != 1:
        raise AuthException('Пользователь не авторизован')
    user_token = db_token[0]
    if update_last_active:
        user_token.last_active = now()
        user_token.save()
    return user_token


def get_current_user(request, raise_exception_on_not_verified=True, update_last_active=False):
    token = get_current_user_token(request, update_last_active)
    user = token.user
    if raise_exception_on_not_verified and not user.verified:
        raise EmailConfirmationRequiredException('Необходимо подтвердить email')
    return user


def get_user_by_token(user_token):
    user_token = UserToken.objects.filter(token=user_token).first()
    return user_token.user if user_token is not None else None


def generate_auth_token(length):
    return token_urlsafe(length)[:length]


def hash_password(password):
    return hashlib.md5(password.encode()).hexdigest()


def form_auth_response(token, user=None):
    response = {
        'token': token,
    }
    user_system_info = {}
    if user is None:
        user = get_user_by_token(token)
    if not user.verified:
        user_system_info['email_verified'] = False
    if len(user_system_info.keys()) > 0:
        response['user_system_info'] = user_system_info
    return response
