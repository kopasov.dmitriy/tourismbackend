from ..exceptions.exceptions import CommonException
from ..models.region import Region


def get_region_by_id(region_id):
    if region_id is None:
        raise CommonException('Не указан идентификатор региона')
    return Region.objects.filter(id=region_id).first()


def get_existing_region_by_id(region_id):
    region = get_region_by_id(region_id)
    if region is None:
        raise CommonException(f'Регион с id {region_id} не найден')
    return region
