from ..exceptions.exceptions import CommonException
from ..models.user import User
from ..models.user_role import UserRole
from .email_functions import validate_email
from datetime import date


def check_user_can_do_action(user, required_action, raise_exception=True):
    roles = UserRole.objects.filter(user=user)
    if len(roles) == 0:
        raise CommonException('У пользователя нет роли')
    for role in roles:
        if hasattr(role.role, required_action) and getattr(role.role, required_action):
            return
    if raise_exception:
        raise CommonException('Для совершения данного действия пользователь не имеет прав')
    else:
        return False


def find_user_by_email(email, raise_exception_on_incorrect_user=True):
    validate_email(email)
    users = User.objects.filter(email=email)
    if len(users) != 1:
        if raise_exception_on_incorrect_user:
            raise CommonException(f'Неверный адрес электронной почты - {email}')
        else:
            return None
    return users[0]


def convert_date(date_string):
    try:
        date_parts = date_string.split('.')
        _date_string = f'{date_parts[2]}-{date_parts[1]}-{date_parts[0]}'
        _date = date.fromisoformat(_date_string)
    except (ValueError, IndexError):
        raise CommonException(f'Неправильный формат даты. Указано значение - {date_string}. Нужный формат - ДД.ММ.ГГГГ')
    return _date


def validate_date_of_birthday(date_of_birthday):
    cur_date = date.today()
    if date_of_birthday > cur_date:
        raise CommonException('Указанная дата рождения больше текущей даты, что недопустимо')
    min_date = date(1880, 1, 1)
    if date_of_birthday < min_date:
        raise CommonException('Указана слишком ранняя дата рождения. Наличие таких старых людей весьма сомнительно')
