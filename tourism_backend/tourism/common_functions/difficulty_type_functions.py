from ..exceptions.exceptions import CommonException
from ..models.difficulty_type import DifficultyType


def get_difficulty_type_by_id(difficulty_type_id):
    if difficulty_type_id is None:
        raise CommonException('Не указан индентификатор вида сложности')
    return DifficultyType.objects.filter(id=difficulty_type_id).first()


def get_existing_difficulty_type_by_id(difficulty_type_id):
    difficulty_type = get_difficulty_type_by_id(difficulty_type_id)
    if difficulty_type is None:
        raise CommonException(f'Вид сложности с id {difficulty_type_id} не найден')
    return difficulty_type
