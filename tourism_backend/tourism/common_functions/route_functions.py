from ..exceptions.exceptions import CommonException
from ..models.route import Route


def validate_rating(rating_org, rating_imp, rating_service, rating_price):
    valid_rating_marks = [0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5]
    for _rating in [rating_org, rating_imp, rating_service, rating_price]:
        if _rating is None:
            raise CommonException(
                f'Как минимум одна из оценок не указан. Нужно указать все'
            )
        if _rating not in valid_rating_marks:
            raise CommonException(
                f'Указано неправильное значение оценки: {_rating}. Допустимые значения оценок: {valid_rating_marks}'
            )


def rate(obj, rating_org, rating_imp, rating_service, rating_price):
    avg_rating = (rating_org + rating_imp + rating_service + rating_price) / 4
    number_of_ratings = obj.number_of_ratings
    obj.avg_rating = (obj.avg_rating * number_of_ratings + avg_rating) / (number_of_ratings + 1)
    obj.avg_rating_org = (obj.avg_rating_org * number_of_ratings + rating_org) / (number_of_ratings + 1)
    obj.avg_rating_imp = (obj.avg_rating_imp * number_of_ratings + rating_imp) / (number_of_ratings + 1)
    obj.avg_rating_service = (obj.avg_rating_service * number_of_ratings + rating_service) / (number_of_ratings + 1)
    obj.avg_rating_price = (obj.avg_rating_price * number_of_ratings + rating_price) / (number_of_ratings + 1)
    obj.number_of_ratings += 1


def get_route_by_id(route_id):
    if route_id is None:
        raise CommonException('Не указан идентификатор маршрута')
    return Route.objects.filter(id=route_id).first()


def get_existing_route_by_id(route_id):
    route = get_route_by_id(route_id)
    if route is None:
        raise CommonException(f'Маршрут с id {route_id} не найден')
    return route
