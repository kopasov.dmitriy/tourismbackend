from ..exceptions.exceptions import CommonException
from ..models.target_group import TargetGroup


def get_target_group_by_id(target_group_id):
    if target_group_id is None:
        raise CommonException('Не указан идентификатор целевой группы')
    return TargetGroup.objects.filter(id=target_group_id).first()


def get_existing_target_group_by_id(target_group_id):
    target_group = get_target_group_by_id(target_group_id)
    if target_group is None:
        raise CommonException(f'Целевая группа с id {target_group_id} не найдена')
    return target_group
