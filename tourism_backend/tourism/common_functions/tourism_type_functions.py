from ..exceptions.exceptions import CommonException
from ..models.tourism_type import TourismType


def get_tourism_type_by_id(tourism_type_id):
    if tourism_type_id is None:
        raise CommonException('Не указан идентификатор типа туризма')
    return TourismType.objects.filter(id=tourism_type_id).first()


def get_existing_tourism_type_by_id(tourism_type_id):
    tourism_type = get_tourism_type_by_id(tourism_type_id)
    if tourism_type is None:
        raise CommonException(f'Тип туризма с id {tourism_type_id} не найден')
    return tourism_type


def get_existing_tourism_types_by_id_list(tourism_type_ids):
    for tourism_type_id in tourism_type_ids:
        get_existing_tourism_type_by_id(tourism_type_id)
    return TourismType.objects.filter(id__in=tourism_type_ids)
