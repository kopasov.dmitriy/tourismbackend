from ..exceptions.exceptions import CommonException
from django.conf import settings
from django.core.mail import send_mail
from validate_email import validate_email as _validate_email_


def send_text_email(subject, message, recipient_list):
    send_mail(subject,
              message,
              settings.EMAIL_HOST_USER,
              recipient_list)


def send_text_email_as_html(subject, message, recipient_list, html_message):
    send_mail(subject,
              message,
              settings.EMAIL_HOST_USER,
              recipient_list,
              html_message=html_message)


def validate_email(email):
    if email is None:
        raise CommonException('Не указан email')
    if not _validate_email_(email):
        raise CommonException('Указанный email имеет неправильный формат')
    # Для этой проверки нужно открывать порт, но на сервере есть проблемы с доступом к портам
    # В целом можно обойтись без этой проверки
    # if not _validate_email_(email, verify=True):
    # raise CommonException('Указанный email не существует')
