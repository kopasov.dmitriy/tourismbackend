from ..encrypter import encrypter
from ..exceptions.exceptions import CommonException
from ..google_drive import init_google_drive
from ..google_drive.init_google_drive import GOOGLE_DRIVE_LOCAL_FOLDER
from PIL import Image
from datetime import datetime
from googleapiclient.http import MediaFileUpload
import base64
import math
import mimetypes
import os
import uuid

MAX_FILE_UPLOAD_SIZE = 5 * 1024 * 1024  # 5 MB


def _save_local_temp_file_(original_file_bytes, extension, encrypting_key, is_base64=False):
    temp_file_name = GOOGLE_DRIVE_LOCAL_FOLDER / f'temp_{str(uuid.uuid4())}{extension}'
    with open(temp_file_name, 'wb') as file:
        if is_base64:
            if encrypting_key is None:
                file_bytes = base64.decodebytes(original_file_bytes.encode())
            else:
                file_bytes = encrypter.encrypt_bytes(encrypting_key, base64.decodebytes(original_file_bytes.encode()))
        else:
            if encrypting_key is not None:
                file_bytes = encrypter.encrypt_bytes(encrypting_key, original_file_bytes)
            else:
                file_bytes = original_file_bytes
        file.write(file_bytes)

    return temp_file_name


def _resize_photo_(full_photo_name, small_photo_size):
    small_photo_name = full_photo_name
    if small_photo_size is not None:
        full_image = Image.open(full_photo_name)
        h, w = full_image.size
        size_factor = max(h, w)
        if size_factor > small_photo_size:
            w = math.ceil(w / size_factor * small_photo_size)
            h = math.ceil(h / size_factor * small_photo_size)
            small_image = full_image.resize((h, w), Image.BILINEAR)
            small_photo_name = str(small_photo_name).replace('.', '_small.', 1)
            small_image.save(small_photo_name, quality=90, optimize=True)
    return small_photo_name


def upload_file(file_size=None, file_content_type=None, file_bytes=None, file_base64=None,
                folder=None, encrypting_key=None, small_photo_size=None):
    if file_base64 is not None:
        _check_file_size_(len(file_base64) * 0.75)
    else:
        _check_file_size_(file_size)
    _check_file_mimetype_(file_content_type)

    if file_base64 is not None:
        temp_file_name = _save_local_temp_file_(file_base64,
                                                _get_extension_by_content_type_(file_content_type),
                                                encrypting_key,
                                                True)
    else:
        temp_file_name = _save_local_temp_file_(file_bytes,
                                                _get_extension_by_content_type_(file_content_type),
                                                encrypting_key)
    small_temp_file_name = _resize_photo_(temp_file_name, small_photo_size) if encrypting_key is None else None

    file_name = _generate_google_drive_file_name_(file_content_type)
    small_file_name = None
    if encrypting_key is not None:
        file_name += '.encrypted'
    else:
        small_file_name = file_name.replace('.', '_small.', 1)
    file = _upload_file_(file_name, temp_file_name, file_content_type, folder)
    if small_temp_file_name is not None:
        if temp_file_name != small_temp_file_name:
            small_file = _upload_file_(small_file_name, small_temp_file_name, file_content_type, folder)
            os.remove(small_temp_file_name)
        else:
            small_file = file
        os.remove(temp_file_name)
    else:
        small_file = {'id': None}
    return file['id'], small_file['id']


def _upload_file_(file_name, temp_file_name, mime_type, folder):
    drive_service = init_google_drive.init()
    file_metadata = {
        'name': file_name,
        'mimeType': mime_type,
        'parents': [folder]
    }
    media = MediaFileUpload(temp_file_name,
                            mimetype=mime_type,
                            resumable=False)
    file = drive_service.files().create(body=file_metadata,
                                        media_body=media,
                                        fields='id').execute()
    return file
    # drive = init_google_drive.init()
    # gfile = drive.CreateFile({'parents': [{'id': init_google_drive.PHOTO_FOLDER_ID}],
    # 'title': file_name})
    # gfile.SetContentFile(temp_file_name)
    # gfile.Upload()

    # return gfile


def delete_file(file_id):
    drive_service = init_google_drive.init()
    drive_service.files().delete(fileId=file_id).execute()


def _generate_google_drive_file_name_(file_content_type):
    extension = _get_extension_by_content_type_(file_content_type)
    current_time = datetime.now()
    return f'{current_time.year}-{current_time.month:0>2d}-{current_time.day:0>2d}-{current_time.hour:0>2d}-' \
           f'{current_time.minute:0>2d}-{current_time.second:0>2d}-{current_time.microsecond}{extension}'


def _check_file_size_(file_size):
    if file_size > MAX_FILE_UPLOAD_SIZE:
        raise CommonException('Превышен допустимый объём загрузки файлов - '
                              f'{int(MAX_FILE_UPLOAD_SIZE / 1024 / 1024)} MB')


def _check_file_mimetype_(file_content_type):
    if file_content_type not in ['image/jpeg', 'image/png']:
        raise CommonException('Недопустимый формат изображениия')


def _get_extension_by_content_type_(file_content_type):
    return mimetypes.guess_extension(file_content_type)


def get_google_drive_file_access_link(file_id):
    if file_id is None:
        return None
    elif '://' in file_id:
        return file_id
    else:
        return f'https://drive.google.com/uc?export=view&id={file_id}'
