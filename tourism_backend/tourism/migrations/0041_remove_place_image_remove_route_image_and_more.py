# Generated by Django 4.1.1 on 2022-12-19 14:16

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tourism', '0040_role_can_admin'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='place',
            name='image',
        ),
        migrations.RemoveField(
            model_name='route',
            name='image',
        ),
        migrations.RemoveField(
            model_name='user',
            name='image',
        ),
    ]
