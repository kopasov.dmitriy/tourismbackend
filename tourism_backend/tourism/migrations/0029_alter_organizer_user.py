# Generated by Django 4.1.1 on 2022-09-13 12:29

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('tourism', '0028_alter_organizer_options_organizer_created_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='organizer',
            name='user',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='tourism.user', verbose_name='Пользователь'),
        ),
    ]
