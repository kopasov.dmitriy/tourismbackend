# Generated by Django 4.1.1 on 2022-10-15 16:26

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('tourism', '0031_useruploadedfile_type'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='usertoken',
            name='created',
        ),
        migrations.AddField(
            model_name='usertoken',
            name='device_name',
            field=models.CharField(default='TEST', max_length=200, verbose_name='Устройство'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='usertoken',
            name='last_active',
            field=models.DateTimeField(default=django.utils.timezone.now, verbose_name='Последняя активность'),
            preserve_default=False,
        ),
    ]
