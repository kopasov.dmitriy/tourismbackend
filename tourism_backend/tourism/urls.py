from .api import auth, user, file, email, route, password, model_fields, place, organizer
from django.urls import path

urlpatterns = [
    # AUTH
    path('auth/register', auth.register_user),
    path('auth/google', auth.auth_with_google),
    path('auth/login', auth.login),
    path('auth/logout', auth.logout),
    path('auth/check_token', auth.check_token),

    # PASSWORD
    path('password/reset', password.reset_password),
    path('password/check_request', password.check_password_request),
    path('password/create', password.create_new_password),

    # USER
    path('user', user.get_user_info),
    path('user/update', user.update_user_info),
    path('user/roles', user.get_available_roles),
    path('user/role', user.choose_role),
    path('user/profile_image', user.update_user_profile_image),
    path('user/permissions', user.get_permissions),
    path('user/favorite/routes', user.get_favorite_routes),
    path('user/favorite/places', user.get_favorite_places),
    path('user/tokens', user.get_user_tokens),
    path('user/tokens/delete', user.delete_tokens),
    path('user/tokens/delete/<int:token_id>', user.delete_token),

    # EMAIL
    path('email/ask_confirmation', email.ask_user_email_confirmation),
    path('email/confirm', email.confirm_user_email),
    path('email/<str:email>', email.send_email),

    # FILE
    path('file', file.upload_file),
    path('file/confidential', file.upload_file_confidential),
    path('file/confidential/<str:file_id>', file.get_confidential_file),
    path('file/usage', file.get_files_usage),
    path('file/<str:file_id>', file.delete_file),
    # path('file/refresh', file.refresh_gdrive_token),

    # ROUTE
    path('route', route.create_route),
    path('route/<int:route_id>', route.get_route_info),
    path('route/find', route.find_routes),
    path('route/map', route.find_routes_on_map),
    path('route/favorite/<int:route_id>', route.add_route_to_favorite),
    path('route/favorite/<int:route_id>/delete', route.delete_route_from_favorite),
    path('route/<int:route_id>/rate', route.rate_route),
    path('route/<int:route_id>/reviews', route.get_route_reviews),
    path('route/regions', route.get_regions),
    path('route/target_groups', route.get_target_groups),
    path('route/tourism_types', route.get_tourism_types),
    path('route/difficulty_types', route.get_difficulty_types),
    path('route/<int:route_id>/participate', route.participate),
    path('route/<int:route_id>/participants', route.get_participants),
    path('route/confirm_participation/<int:route_participant_id>', route.confirm_participation),
    path('route/cancel_participation/<int:route_participant_id>', route.cancel_participation),

    # PLACE
    path('place', place.create_place),
    path('place/<int:place_id>', place.get_place_info),
    path('place/find', place.find_places),
    path('place/random', place.get_random_place),
    path('place/favorite/<int:place_id>', place.add_place_to_favorite),
    path('place/favorite/<int:place_id>/delete', place.delete_place_from_favorite),
    path('place/<int:place_id>/rate', place.rate_place),
    path('place/<int:place_id>/reviews', place.get_place_reviews),

    # ORGANIZER
    path('organizer', organizer.find_organizers),
    path('organizer/<int:organizer_id>', organizer.get_organizer_info),
    path('organizer/info', organizer.get_current_organizer_info),
    path('organizer/create', organizer.create_organization),
    path('organizer/attach/<str:document_type>', organizer.attach_document),

    # MODEL FIELDS
    path('model_fields/<str:model>', model_fields.get_model_fields)
]
