class LocalBaseException(Exception):
    def __init__(self, message, status, key):
        self.status = status
        self.key = key
        super().__init__(message)


class CommonException(LocalBaseException):
    def __init__(self, message):
        super().__init__(message, 400, 'error')


class AuthException(LocalBaseException):
    def __init__(self, message):
        super().__init__(message, 401, 'error')


class EmailConfirmationRequiredException(LocalBaseException):
    def __init__(self, message):
        super().__init__(message, 403, 'error')
