from ..common_functions.auth_functions import form_auth_response, get_current_user_token
from ..services import auth_service, email_service
from ._base_decorators import handle_request, require_DELETE
from .common_request_functions import get_string, get_int, get_device_name
from django.views.decorators.http import require_POST, require_GET


@require_POST
@handle_request
def register_user(request):
    email = get_string(request.POST, 'email')
    phone_number = get_string(request.POST, 'phone_number')
    password = get_string(request.POST, 'password')
    first_name = get_string(request.POST, 'first_name')
    surname = get_string(request.POST, 'surname')
    additional_name = get_string(request.POST, 'additional_name')
    country = get_string(request.POST, 'country')
    region_id = get_int(request.POST, 'region_id')
    city = get_string(request.POST, 'city')
    date_of_birthday = get_string(request.POST, 'date_of_birthday')

    device_name = get_device_name(request)

    user_token = auth_service.register_user(email,
                                            phone_number,
                                            password,
                                            first_name,
                                            surname,
                                            additional_name,
                                            country,
                                            region_id,
                                            city,
                                            date_of_birthday,
                                            device_name)
    email_service.ask_user_email_confirmation(user_token.user)

    return form_auth_response(user_token.token)


@require_POST
@handle_request
def login(request):
    email = get_string(request.POST, 'email')
    password = get_string(request.POST, 'password')
    device_name = get_device_name(request)
    user_token = auth_service.login(email, password, device_name)
    return form_auth_response(user_token.token)


@require_POST
@handle_request
def auth_with_google(request):
    google_token = get_string(request.POST, 'google_token')
    device_name = get_device_name(request)
    user_token = auth_service.auth_with_google(google_token, device_name)
    return form_auth_response(user_token.token)


@require_DELETE
@handle_request
def logout(request):
    user_token = get_current_user_token(request, True)
    auth_service.logout(user_token)
    return {
        'message': 'Успешный выход из системы',
    }


@require_GET
@handle_request
def check_token(request):
    user_token = get_current_user_token(request, True)
    return form_auth_response(user_token.token, user_token.user)
