from ..converters import\
    converter_difficulty_type,\
    converter_organizer,\
    converter_place, \
    converter_rating,\
    converter_region,\
    converter_review,\
    converter_role,\
    converter_route,\
    converter_route_participant,\
    converter_target_group,\
    converter_tourism_type,\
    converter_user,\
    converter_user_token
from ._base_decorators import handle_request
from django.views.decorators.http import require_GET


def combine_fields_description(names, descriptions):
    result = dict()
    for name, description in zip(names, descriptions):
        result[name] = description
    return result


@require_GET
@handle_request
def get_model_fields(_, model):
    fields = []
    if model == 'difficulty_type':
        fields = combine_fields_description(
            converter_difficulty_type.difficulty_types_all_fields,
            converter_difficulty_type.difficulty_types_all_fields_description,
        )
    elif model == 'organizer':
        fields = combine_fields_description(
            converter_organizer.organizers_all_fields,
            converter_organizer.organizers_all_fields_description
        )
    elif model == 'place':
        fields = combine_fields_description(
            converter_place.places_all_fields,
            converter_place.places_all_fields_description
        )
    elif model == 'rating':
        fields = combine_fields_description(
            converter_rating.rating_all_fields,
            converter_rating.rating_all_fields_description
        )
    elif model == 'region':
        fields = combine_fields_description(
            converter_region.regions_all_fields,
            converter_region.regions_all_fields_description
        )
    elif model == 'review':
        fields = combine_fields_description(
            converter_review.review_all_fields,
            converter_review.review_all_fields_description
        )
    elif model == 'role':
        fields = combine_fields_description(
            converter_role.roles_all_fields,
            converter_role.roles_all_fields_description
        )
    elif model == 'route':
        fields = combine_fields_description(
            converter_route.routes_all_fields,
            converter_route.routes_all_fields_description
        )
    elif model == 'route_participant':
        fields = combine_fields_description(
            converter_route_participant.route_participants_all_fields,
            converter_route_participant.route_participants_all_fields_description
        )
    elif model == 'target_group':
        fields = combine_fields_description(
            converter_target_group.target_groups_all_fields,
            converter_target_group.target_groups_all_fields_description
        )
    elif model == 'tourism_type':
        fields = combine_fields_description(
            converter_tourism_type.tourism_types_all_fields,
            converter_tourism_type.tourism_types_all_fields_description
        )
    elif model == 'user':
        fields = combine_fields_description(
            converter_user.users_all_fields,
            converter_user.users_all_fields_description
        )
    elif model == 'user_token':
        fields = combine_fields_description(
            converter_user_token.user_tokens_all_fields,
            converter_user_token.user_tokens_all_fields_description
        )
    return {
        'fields': fields,
    }
