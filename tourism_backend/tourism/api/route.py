from ..common_functions import route_functions
from ..converters import converter
from ..services import route_service
from ._base_decorators import handle_request, require_action, require_auth, optional_auth, require_PUT, \
    require_DELETE, require_PATCH, require_organizer
from .common_request_functions import get_string, get_int, get_offset_and_limit, get_float, get_list_of_ints, get_bool
from django.views.decorators.http import require_POST, require_GET


@require_GET
@handle_request
def get_regions(request):
    name = get_string(request.GET, 'name')
    regions = route_service.get_regions(name)
    return {
        'regions': converter.convert_list(regions, request),
    }


@require_GET
@handle_request
def get_target_groups(request):
    target_groups = route_service.get_target_groups()
    return {
        'target_groups': converter.convert_list(target_groups, request),
    }


@require_GET
@handle_request
def get_tourism_types(request):
    name = get_string(request.GET, 'name')
    tourism_types = route_service.get_tourism_types(name)
    return {
        'tourism_types': converter.convert_list(tourism_types, request),
    }


@require_GET
@handle_request
def get_difficulty_types(request):
    difficulty_types = route_service.get_difficulty_types()
    return {
        'difficulty_types': converter.convert_list(difficulty_types, request),
    }


@require_POST
@handle_request
@require_auth
@require_action('can_add_routes')
def create_route(request, user):
    name = get_string(request.POST, 'name')
    description = get_string(request.POST, 'description')
    location_latitude = get_float(request.POST, 'location_latitude')
    location_longitude = get_float(request.POST, 'location_longitude')
    country = get_string(request.POST, 'country')
    region_id = get_int(request.POST, 'region_id')
    city = get_string(request.POST, 'city')
    cost = get_float(request.POST, 'cost')
    distance = get_int(request.POST, 'distance')
    min_tourists = get_int(request.POST, 'min_tourists')
    max_tourists = get_int(request.POST, 'max_tourists')
    image = (get_string(request.POST, 'image'), request)
    target_group_id = get_int(request.POST, 'target_group_id')
    difficulty_type_id = get_int(request.POST, 'difficulty_type_id')
    tourism_type_ids = get_list_of_ints(request.POST, 'tourism_type_ids')
    start_date = get_string(request.POST, 'start_date')
    end_date = get_string(request.POST, 'end_date')

    route = route_service.create_route(user, name, description,
                                       location_latitude, location_longitude,
                                       country, region_id, city, cost, distance,
                                       min_tourists, max_tourists, image,
                                       target_group_id, difficulty_type_id, tourism_type_ids, start_date, end_date)

    return {
        'route_info': converter.convert(route, request),
    }


@require_GET
@handle_request
def get_route_info(request, route_id):
    route = route_functions.get_existing_route_by_id(route_id)
    return {
        'route_info': converter.convert(route, request),
    }


@require_GET
@handle_request
def find_routes(request):
    name = get_string(request.GET, 'name')
    org_ids = get_list_of_ints(request.GET, 'org_ids')
    reg_ids = get_list_of_ints(request.GET, 'reg_ids')
    target_group_id = get_int(request.GET, 'target_group_id')
    tourism_type_ids = get_list_of_ints(request.GET, 'tourism_type_ids')
    difficulty_type_ids = get_list_of_ints(request.GET, 'difficulty_type_ids')
    cost_greater_than = get_float(request.GET, 'cost_greater_than')
    cost_less_than = get_float(request.GET, 'cost_less_than')
    distance_greater_than = get_int(request.GET, 'distance_greater_than')
    distance_less_than = get_int(request.GET, 'distance_less_than')
    start_date_greater_than = get_string(request.GET, 'start_date_greater_than')
    start_date_less_than = get_string(request.GET, 'start_date_less_than')
    end_date_greater_than = get_string(request.GET, 'end_date_greater_than')
    end_date_less_than = get_string(request.GET, 'end_date_less_than')

    offset, limit = get_offset_and_limit(request)

    routes, total = route_service.find_routes(name, org_ids, reg_ids, target_group_id,
                                              tourism_type_ids, difficulty_type_ids, cost_greater_than, cost_less_than,
                                              distance_greater_than, distance_less_than,
                                              start_date_greater_than, start_date_less_than,
                                              end_date_greater_than, end_date_less_than,
                                              offset, limit)

    return {
        'routes_info': converter.convert_list(routes, request),
        'total': total,
    }


@require_GET
@handle_request
@optional_auth
def find_routes_on_map(request, user):
    name = get_string(request.GET, 'name')
    exclude_ids = get_string(request.GET, 'exclude_ids')

    if exclude_ids is not None:
        exclude_ids = [int(_id) for _id in exclude_ids.split(',')]
    latitude = get_float(request.GET, 'latitude', 1000)
    latitude_delta = get_float(request.GET, 'latitude_delta', 40)
    longitude = get_float(request.GET, 'longitude', 1000)
    longitude_delta = get_float(request.GET, 'longitude_delta', 60)
    if latitude == 1000 or longitude == 1000:
        if user is not None and user.region is not None:
            latitude, longitude = user.region.latitude, user.region.longitude
        else:
            latitude, longitude = 55, 90
    if longitude_delta < 0:
        longitude_delta += 360

    result = route_service.find_routes_on_map(latitude,
                                              longitude,
                                              latitude_delta,
                                              longitude_delta,
                                              name,
                                              exclude_ids)

    if longitude_delta > 20:
        return {
            'regions': converter.convert_list(result, request),
        }
    return {
        'routes': converter.convert_list(result, request),
    }


@require_PUT
@handle_request
@require_auth
def add_route_to_favorite(_, user, route_id):
    route_service.add_route_to_favorite(user, route_id)
    return {
        'message': 'Успешно добавлено',
    }


@require_DELETE
@handle_request
@require_auth
def delete_route_from_favorite(_, user, route_id):
    route_service.delete_route_from_favorite(user, route_id)
    return {
        'message': 'Успешно удалено',
    }


@require_POST
@handle_request
@require_auth
def rate_route(request, user, route_id):
    rating_org = get_float(request.POST, 'rating_org')
    rating_imp = get_float(request.POST, 'rating_imp')
    rating_service = get_float(request.POST, 'rating_service')
    rating_price = get_float(request.POST, 'rating_price')

    review_text = get_string(request.POST, 'review_text')

    route_service.rate_route(user, route_id, rating_org, rating_imp, rating_service, rating_price, review_text)
    return {
        'message': 'Успешно оценено',
    }


@require_GET
@handle_request
def get_route_reviews(request, route_id):
    offset, limit = get_offset_and_limit(request)

    reviews, total = route_service.get_route_reviews(route_id, offset, limit)
    return {
        'reviews': converter.convert_list(reviews, request),
        'total': total,
    }


@require_PATCH
@handle_request
@require_auth
def participate(request,  user, route_id):
    reserved_places = get_int(request.GET, 'reserved_places')
    message = get_string(request.GET, 'message', '')

    route_service.participate(route_id, user, reserved_places, message)
    return {
        'message': 'Заявка на участие отправлена',
    }


@require_GET
@handle_request
@require_organizer(True)
def get_participants(request, organizer, route_id):
    offset, limit = get_offset_and_limit(request)
    confirmed = get_bool(request.GET, 'confirmed')

    route_participants, total = route_service.get_participants(route_id, organizer, confirmed, offset, limit)
    return {
        'route_participants': converter.convert_list(route_participants, request),
        'total': total,
    }


@require_PATCH
@handle_request
@require_organizer(True)
def confirm_participation(request, organizer, route_participant_id):
    confirmed_places = get_int(request.GET, 'confirmed_places')
    route_participation = route_service.confirm_participation(route_participant_id, organizer, confirmed_places)
    return {
        'route_participation': converter.convert(route_participation, request),
    }


@require_DELETE
@handle_request
@require_organizer(True)
def cancel_participation(_, organizer, route_participant_id):
    route_service.cancel_participation(route_participant_id, organizer)
    return {
        'message': 'Участие отклонено',
    }
