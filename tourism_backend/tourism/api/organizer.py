from ..common_functions.organizer_functions import get_current_organizer_or_none
from ..converters import converter
from ..services import organizer_service
from ._base_decorators import handle_request, require_auth, require_organizer
from .common_request_functions import get_string, get_offset_and_limit, get_bool
from django.views.decorators.http import require_GET, require_POST


@require_GET
@handle_request
def get_organizer_info(request, organizer_id):
    organizer = organizer_service.get_organizer_info(organizer_id)
    return {
        'organizer_info': converter.convert(organizer, request),
    }


@require_GET
@handle_request
@require_auth
def get_current_organizer_info(request, user):
    return {
        'organizer_info': converter.convert(get_current_organizer_or_none(user), request),
    }


@require_POST
@handle_request
@require_auth
def create_organization(request, user):
    name = get_string(request.POST, 'name')
    passport_series = get_string(request.POST, 'passport_series')
    passport_number = get_string(request.POST, 'passport_number')
    passport_issue_date = get_string(request.POST, 'passport_issue_date')
    passport_issue_authority = get_string(request.POST, 'passport_issue_authority')
    individual_tax_number = get_string(request.POST, 'individual_tax_number')

    organizer = organizer_service.create_organization(user, name, passport_series, passport_number, passport_issue_date,
                                                      passport_issue_authority, individual_tax_number)
    return {
        'organizer_info': converter.convert(organizer, request),
    }


@require_POST
@handle_request
@require_organizer(False)
def attach_document(request, organizer, document_type):
    organizer = organizer_service.attach_document(organizer, request, document_type)
    return {
        'organizer_info': converter.convert(organizer, request),
    }


@require_GET
@handle_request
def find_organizers(request):
    name = get_string(request.GET, 'name')
    verified = get_bool(request.GET, 'verified')
    offset, limit = get_offset_and_limit(request)

    organizers, total = organizer_service.find_organizers(name, verified, offset, limit)
    return {
        'organizers': converter.convert_list(organizers, request),
        'total': total,
    }
