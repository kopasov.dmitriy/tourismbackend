from ..common_functions.auth_functions import get_current_user
from ..common_functions.organizer_functions import get_current_organizer
from ..common_functions.user_functions import check_user_can_do_action
from ..exceptions.exceptions import LocalBaseException, CommonException
from .common_request_functions import is_dev_server
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from functools import wraps

require_PATCH = require_http_methods(['PATCH'])
require_DELETE = require_http_methods(['DELETE'])
require_PUT = require_http_methods(['PUT'])


def handle_request(function):
    def inner_function(*args, **kwargs):
        try:
            return JsonResponse(function(*args, **kwargs))
        except LocalBaseException as error:
            return JsonResponse({error.key: str(error)}, status=error.status)
        except (Exception,) as e:
            if is_dev_server():
                raise e
            else:
                return JsonResponse({'error': 'Неизвестная ошибка', 'error_info': f'{str(e)}'}, status=500)
    return inner_function


def require_action(*actions):
    def decorator(function):
        @wraps(function)
        def inner_function(request, user, *args, **kwargs):
            for action in actions:
                check_user_can_do_action(user, action)
            return function(request, user, *args, **kwargs)
        return inner_function
    return decorator


def require_auth(function):
    def inner_function(request, *args, **kwargs):
        user = get_current_user(request, True, True)
        return function(request, user, *args, **kwargs)
    return inner_function


def optional_auth(function):
    def inner_function(request, *args, **kwargs):
        user = None
        try:
            user = get_current_user(request, True, True)
        except (Exception,):
            pass
        return function(request, user, *args, **kwargs)
    return inner_function


def require_organizer(verified=None):
    def decorator(function):
        @wraps(function)
        def inner_function(request, *args, **kwargs):
            organizer = get_current_organizer(request)
            if verified is not None:
                if verified and not organizer.verified:
                    raise CommonException('Организатор должен быть подтверждён')
                if not verified and organizer.verified:
                    raise CommonException('Организатор уже подтверждён')
            return function(request, organizer, *args, **kwargs)

        return inner_function

    return decorator
