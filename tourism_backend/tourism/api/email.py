from ..common_functions.auth_functions import get_current_user
from ..services import email_service
from ._base_decorators import handle_request, require_PUT, require_action
from .common_request_functions import get_string
from django.views.decorators.http import require_POST


@require_PUT
@handle_request
def ask_user_email_confirmation(request):
    user = get_current_user(request, False, True)
    email_service.ask_user_email_confirmation(user)
    return {
        'message': 'Запрос на подтверждение успешно создан',
    }


@require_POST
@handle_request
def confirm_user_email(request):
    user = get_current_user(request, False, True)
    key = get_string(request.POST, 'key')
    email_service.confirm_user_email(user, key)
    return {
        'message': 'Электронная почта успешно подверждена',
    }


@require_PUT
@handle_request
@require_action('can_admin')
def send_email(_, email):
    email_service.send_email(email)
    return {
        'message': 'Письмо успешно отправлено',
    }
