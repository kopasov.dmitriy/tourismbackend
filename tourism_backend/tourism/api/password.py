from ..services import password_service
from ._base_decorators import handle_request
from .common_request_functions import get_string
from django.views.decorators.http import require_POST, require_GET


@require_POST
@handle_request
def reset_password(request):
    email = get_string(request.POST, 'email')
    password_service.reset_password(email)
    return {
        'message': 'Запрос на сброс пароля создан, информация по восстановлению пароля отправлена на почту',
    }


@require_GET
@handle_request
def check_password_request(request):
    email = get_string(request.GET, 'email')
    key = get_string(request.GET, 'key')
    password_service.check_password_request(email, key)
    return {
        'message': 'Указан правильный ключ',
    }


@require_POST
@handle_request
def create_new_password(request):
    key = get_string(request.POST, 'key')
    password = get_string(request.POST, 'password')
    password_service.create_new_password(key, password)
    return {
        'message': 'Новый пароль успешно создан',
    }
