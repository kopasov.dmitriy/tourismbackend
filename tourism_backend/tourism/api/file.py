from ..exceptions.exceptions import CommonException
from ..google_drive.init_google_drive import CONFIDENTIAL_FOLDER
from ..services import file_service
from ._base_decorators import handle_request, require_auth, require_action, require_DELETE
from .common_request_functions import get_string
from django.views.decorators.http import require_POST, require_GET
from googleapiclient.errors import HttpError


@require_POST
@handle_request
def upload_file(request):
    file = file_service.upload_file_common(request)
    return {
        'file_id': file.file_id,
    }


@require_POST
@handle_request
def upload_file_confidential(request):
    file = file_service.upload_file_common(request, CONFIDENTIAL_FOLDER, True)
    return {
        'file_id': file.file_id,
    }


@require_GET
@handle_request
@require_auth
@require_action('can_view_confidential_data')
def get_confidential_file(_, user, file_id):
    file_content_as_base64 = file_service.get_confidential_file(user, file_id)
    return {
        'file_as_base64': file_content_as_base64,
    }


@require_DELETE
@handle_request
@require_auth
@require_action('can_admin')
def delete_file(request, __, file_id):
    try:
        _type = get_string(request.GET, 'type', '')
        file_service.delete_file(file_id, _type)
    except HttpError as e:
        raise CommonException(e.error_details)
    return {
        'message': 'Файл успешно удалён',
    }


@require_GET
@handle_request
@require_auth
@require_action('can_admin')
def get_files_usage(_, __):
    files = file_service.get_files_usage()
    return {
        'files': files,
    }
