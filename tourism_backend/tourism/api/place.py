from ..common_functions import place_functions
from ..converters import converter
from ..services import place_service
from ._base_decorators import handle_request, require_action, require_auth, require_PUT, require_DELETE
from .common_request_functions import get_string, get_int, get_offset_and_limit, get_float
from django.views.decorators.http import require_POST, require_GET


@require_POST
@handle_request
@require_auth
@require_action('can_add_places')
def create_place(request, user):
    creator_email = user.email

    name = get_string(request.POST, 'name')
    description = get_string(request.POST, 'description')
    short_description = get_string(request.POST, 'short_description')
    location_latitude = get_float(request.POST, 'location_latitude')
    location_longitude = get_float(request.POST, 'location_longitude')
    country = get_string(request.POST, 'country')
    region_id = get_int(request.POST, 'region_id')
    city = get_string(request.POST, 'city')
    image = (get_string(request.POST, 'image'), request)

    place = place_service.create_place(name, short_description, description,
                                       location_latitude, location_longitude,
                                       country, region_id, city, image, creator_email)

    return {
        'place_info': converter.convert(place, request),
    }


@require_GET
@handle_request
def find_places(request):
    name = get_string(request.GET, 'name')
    offset, limit = get_offset_and_limit(request)

    places, total = place_service.find_places(name, offset, limit)

    return {
        'places_info': converter.convert_list(places, request),
        'total': total,
    }


@require_GET
@handle_request
def get_place_info(request, place_id):
    place = place_functions.get_existing_place_by_id(place_id)
    return {
        'place_info': converter.convert(place, request),
    }


@require_GET
@handle_request
def get_random_place(request):
    place = place_service.get_random_place()
    return {
        'place_info': converter.convert(place, request),
    }


@require_PUT
@handle_request
@require_auth
def add_place_to_favorite(_, user, place_id):
    place_service.add_place_to_favorite(user, place_id)
    return {
        'message': 'Успешно добавлено',
    }


@require_DELETE
@handle_request
@require_auth
def delete_place_from_favorite(_, user, place_id):
    place_service.delete_place_from_favorite(user, place_id)
    return {
        'message': 'Успешно удалено',
    }


@require_POST
@handle_request
@require_auth
def rate_place(request, user, place_id):
    rating_org = get_float(request.POST, 'rating_org')
    rating_imp = get_float(request.POST, 'rating_imp')
    rating_service = get_float(request.POST, 'rating_service')
    rating_price = get_float(request.POST, 'rating_price')

    review_text = get_string(request.POST, 'review_text')

    place_service.rate_place(user, place_id, rating_org, rating_imp, rating_service, rating_price, review_text)
    return {
        'message': 'Успешно оценено',
    }


@require_GET
@handle_request
def get_place_reviews(request, place_id):
    offset, limit = get_offset_and_limit(request)

    reviews, total = place_service.get_place_reviews(place_id, offset, limit)
    return {
        'reviews': converter.convert_list(reviews, request),
        'total': total,
    }
