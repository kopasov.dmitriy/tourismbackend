from pathlib import Path
import json
import os


def is_special_test_user(user_email):
    return user_email in read_settings_parameters().get('SPECIAL_TEST_USERS', [])


def is_dev_server():
    return read_settings_parameters()['DEBUG'] == 'True'


def get_secret_key():
    return read_settings_parameters()['SECRET_KEY']


def read_settings_parameters():
    with open(os.path.join(Path(__file__).resolve().parent.parent.parent.parent / 'settings_parameters.json')) \
            as settings_parameters_file:
        return json.load(settings_parameters_file)


def get_value(source, name, default_value):
    return source.get(name, default_value)


def get_int(source, name, default_value=None):
    try:
        int_value = get_value(source, name, default_value)
        if int_value == default_value:
            return default_value
        return int(int_value)
    except (Exception,):
        return default_value


def get_float(source, name, default_value=None):
    try:
        float_value = get_value(source, name, default_value)
        if float_value == default_value:
            return default_value
        return float(float_value)
    except (Exception,):
        return default_value


def get_bool(source, name, default_value=None):
    try:
        bool_value = get_value(source, name, default_value)
        if bool_value == default_value:
            return default_value
        return bool_value.lower() == 'true'
    except (Exception,):
        return default_value


def get_string(source, name, default_value=None):
    try:
        string_value = get_value(source, name, default_value)
        if string_value == default_value or string_value == '':
            return default_value
        return str(string_value).strip()
    except (Exception,):
        return default_value


def get_list_of_ints(source, name, default_value=None):
    try:
        values = get_string(source, name, default_value)
        if values is not default_value:
            values = [int(value) for value in values.split(',')]
            return values
        return []
    except (Exception,):
        return []


def get_fields(request):
    return get_string(request.GET, 'fields', '')


def get_offset_and_limit(request):
    return get_int(request.GET, 'offset', 0), get_int(request.GET, 'limit', 5_000_000)


def get_device_name(request):
    device_name = '@@@@'
    if 'User-Agent' in request.headers:
        device_name = request.headers['User-Agent']
    return device_name
