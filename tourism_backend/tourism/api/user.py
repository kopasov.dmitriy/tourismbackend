from ..converters import converter
from ..services import user_service
from ._base_decorators import handle_request, require_auth, require_DELETE, require_PATCH
from .common_request_functions import get_string, get_int, get_offset_and_limit
from django.views.decorators.http import require_GET, require_POST


@require_GET
@handle_request
@require_auth
def get_user_info(request, user):
    return {
        'user_info': converter.convert(user, request),
    }


@require_GET
@handle_request
def get_available_roles(request):
    roles = user_service.get_available_roles()
    return {
        'roles': converter.convert_list(roles, request),
    }


@require_PATCH
@handle_request
@require_auth
def choose_role(request, user):
    user_service.choose_role(user, get_int(request.GET, 'id'))
    return {
        'message': 'Роль успешно установлена',
    }


@require_POST
@handle_request
@require_auth
def update_user_profile_image(request, user):
    profile_image_link = user_service.update_user_profile_image(user, request)
    return {
        'profile_image_link': profile_image_link,
    }


@require_POST
@handle_request
@require_auth
def update_user_info(request, user):
    first_name = get_string(request.POST, 'first_name')
    surname = get_string(request.POST, 'surname')
    additional_name = get_string(request.POST, 'additional_name')
    phone_number = get_string(request.POST, 'phone_number')
    country = get_string(request.POST, 'country')
    region_id = get_int(request.POST, 'region_id')
    city = get_string(request.POST, 'city')
    date_of_birthday = get_string(request.POST, 'date_of_birthday')

    user = user_service.update_user_info(user,
                                         first_name,
                                         surname,
                                         additional_name,
                                         phone_number,
                                         country,
                                         region_id,
                                         city,
                                         date_of_birthday)

    return {
        'user_info': converter.convert(user, request),
    }


@require_GET
@handle_request
@require_auth
def get_permissions(_, user):
    return {
        'permissions': user_service.get_permissions(user),
    }


@require_GET
@handle_request
@require_auth
def get_favorite_routes(request, user):
    offset, limit = get_offset_and_limit(request)

    routes, total = user_service.get_favorite_routes(user, offset, limit)
    return {
        'favorite_routes': converter.convert_list(routes, request),
        'total': total,
    }


@require_GET
@handle_request
@require_auth
def get_favorite_places(request, user):
    offset, limit = get_offset_and_limit(request)

    places, total = user_service.get_favorite_places(user, offset, limit)
    return {
        'favorite_places': converter.convert_list(places, request),
        'total': total,
    }


@require_GET
@handle_request
@require_auth
def get_user_tokens(request, user):
    offset, limit = get_offset_and_limit(request)

    tokens, total = user_service.get_user_tokens(user, offset, limit)
    return {
        'tokens': converter.convert_list(tokens, request),
        'total': total,
    }


@require_DELETE
@handle_request
@require_auth
def delete_tokens(request, user):
    current_token = request.headers['token']
    deleted_tokens_number = user_service.delete_tokens(user, current_token)
    return {
        'message': f'Удалено токенов: {deleted_tokens_number}',
    }


@require_DELETE
@handle_request
@require_auth
def delete_token(_, user, token_id):
    user_service.delete_token(user, token_id)
    return {
        'message': 'Токен успешно удалён',
    }
