from ..models.target_group import TargetGroup
from django.contrib import admin


@admin.register(TargetGroup)
class TargetGroupAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'description', 'order_priority']
    list_editable = ['name', 'description', 'order_priority']
    search_fields = ['id', 'name', 'description']
    list_filter = []
    list_display_links = None
