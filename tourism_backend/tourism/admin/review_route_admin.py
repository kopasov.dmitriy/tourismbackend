from ..models.review_route import ReviewRoute
from django.contrib import admin


@admin.register(ReviewRoute)
class ReviewRouteAdmin(admin.ModelAdmin):
    list_display = ['id', 'user', 'route', 'rating', 'rating_org', 'rating_imp', 'rating_service', 'rating_price',
                    'review_text', 'created', 'modified']
    list_editable = ['review_text']
    search_fields = ['id', 'user__email', 'user__first_name', 'user__surname', 'route__name', 'review_text']
    list_filter = []
    list_display_links = None
