from ..models.favorite_place import FavoritePlace
from django.contrib import admin


@admin.register(FavoritePlace)
class FavoritePlaceAdmin(admin.ModelAdmin):
    list_display = ['id', 'place', 'user', 'created']
    list_editable = []
    search_fields = ['id', 'user__email', 'user__first_name', 'user__surname', 'place__name']
    list_filter = []
    list_display_links = None
