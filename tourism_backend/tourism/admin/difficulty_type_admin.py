from ..models.difficulty_type import DifficultyType
from django.contrib import admin


@admin.register(DifficultyType)
class DifficultyTypeAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'description', 'order_priority']
    list_editable = ['name', 'description', 'order_priority']
    search_fields = ['id', 'name', 'description']
    list_filter = []
    list_display_links = None
