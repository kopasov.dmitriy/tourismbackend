from ..models.favorite_route import FavoriteRoute
from django.contrib import admin


@admin.register(FavoriteRoute)
class FavoriteRouteAdmin(admin.ModelAdmin):
    list_display = ['id', 'route', 'user', 'created']
    list_editable = []
    search_fields = ['id', 'user__email', 'user__first_name', 'user__surname', 'route__name']
    list_filter = []
    list_display_links = None
