from ..models.route_tourism_type import RouteTourismType
from django.contrib import admin


@admin.register(RouteTourismType)
class RouteTourismTypeAdmin(admin.ModelAdmin):
    list_display = ['id', 'route', 'tourism_type']
    list_editable = ['tourism_type']
    search_fields = ['id', 'route__name']
    list_filter = ['tourism_type__name']
    list_display_links = None
