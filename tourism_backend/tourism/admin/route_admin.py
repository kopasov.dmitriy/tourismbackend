from ..models.route import Route
from django.contrib import admin


@admin.register(Route)
class RouteAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'description', 'location_latitude', 'location_longitude', 'country', 'region', 'city',
                    'cost', 'image_file', 'distance', 'min_tourists',  'max_tourists', 'organizer',
                    'target_group', 'difficulty_type', 'start_date', 'end_date', 'created', 'created_by', 'modified']
    list_editable = ['name', 'description', 'location_latitude', 'location_longitude', 'country', 'region', 'city',
                     'cost', 'image_file', 'distance', 'min_tourists',  'max_tourists', 'organizer',
                     'target_group', 'difficulty_type', 'start_date', 'end_date']
    search_fields = ['id', 'name', 'start_date', 'end_date']
    list_filter = ['region', 'target_group', 'difficulty_type']
    list_display_links = None
