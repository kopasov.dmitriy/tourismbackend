from ..models.rating_place import RatingPlace
from django.contrib import admin


@admin.register(RatingPlace)
class RatingPlaceAdmin(admin.ModelAdmin):
    list_display = ['id', 'place', 'avg_rating', 'avg_rating_org', 'avg_rating_imp', 'avg_rating_service',
                    'avg_rating_price', 'number_of_ratings']
    list_editable = []
    search_fields = ['id', 'place__name']
    list_filter = []
    list_display_links = None
