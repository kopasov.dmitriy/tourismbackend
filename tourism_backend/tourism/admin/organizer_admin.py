from ..models.organizer import Organizer
from django.contrib import admin


@admin.register(Organizer)
class OrganizerAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'user', 'passport_series_enc', 'passport_number_enc', 'passport_series_number_hash',
                    'passport_issue_date_enc', 'passport_issue_authority_enc', 'passport_original_file',
                    'individual_tax_number_enc', 'individual_tax_number_hash', 'individual_tax_number_original_file',
                    'verified', 'created', 'modified']
    list_editable = ['name']
    search_fields = ['id', 'name', 'user__email', 'user__first_name', 'user__surname',
                     'passport_original_file__file_id', 'individual_tax_number_original_file__file_id']
    list_filter = ['verified']
    list_display_links = None
