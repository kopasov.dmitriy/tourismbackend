from ..models.user_password import UserPassword
from django.contrib import admin


@admin.register(UserPassword)
class UserPasswordAdmin(admin.ModelAdmin):
    list_display = ['id', 'user', 'password_hash', 'created', 'modified']
    list_editable = ['password_hash']
    search_fields = ['id', 'user__email', 'user__first_name', 'user__surname']
    list_filter = []
    list_display_links = None
