from ..models.role import Role
from django.contrib import admin


@admin.register(Role)
class RoleAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'can_add_routes', 'can_add_places', 'can_view_confidential_data',
                    'can_admin', 'description', 'image']
    list_editable = ['name', 'can_add_routes', 'can_add_places', 'can_view_confidential_data',
                     'can_admin', 'description', 'image']
    search_fields = ['id', 'name', 'description']
    list_filter = ['can_add_routes', 'can_add_places', 'can_view_confidential_data', 'can_admin']
    list_display_links = None
