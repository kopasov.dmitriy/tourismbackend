from ..models.tourism_type import TourismType
from django.contrib import admin


@admin.register(TourismType)
class TourismTypeAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'description']
    list_editable = ['name', 'description']
    search_fields = ['id', 'name', 'description']
    list_filter = []
    list_display_links = None
