from .difficulty_type_admin import *
from .favorite_place_admin import *
from .favorite_route_admin import *
from .organizer_admin import *
from .place_admin import *
from .rating_place_admin import *
from .rating_route_admin import *
from .region_admin import *
from .review_place_admin import *
from .review_route_admin import *
from .role_admin import *
from .route_admin import *
from .route_participant_admin import *
from .route_tourism_type_admin import *
from .target_group_admin import *
from .tourism_type_admin import *
from .user_admin import *
from .user_confirmation_request_admin import *
from .user_password_admin import *
from .user_role_admin import *
from .user_token_admin import *
from .user_uploaded_file_admin import *
