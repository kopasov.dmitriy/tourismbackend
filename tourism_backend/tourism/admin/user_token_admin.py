from ..models.user_token import UserToken
from django.contrib import admin


@admin.register(UserToken)
class UserTokenAdmin(admin.ModelAdmin):
    list_display = ['id', 'user', 'token', 'device_name', 'last_active']
    list_editable = ['token', 'device_name']
    search_fields = ['id', 'user__email', 'user__first_name', 'user__surname']
    list_filter = []
    list_display_links = None
