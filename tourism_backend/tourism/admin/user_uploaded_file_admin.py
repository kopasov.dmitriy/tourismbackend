from ..models.user_uploaded_file import UserUploadedFile
from django.contrib import admin


@admin.register(UserUploadedFile)
class UserUploadedFileAdmin(admin.ModelAdmin):
    list_display = ['id', 'file_id', 'small_file_id', 'type', 'created', 'created_by']
    list_editable = []
    search_fields = ['id', 'file_id', 'small_file_id', 'created_by']
    list_filter = ['type']
    list_display_links = None
