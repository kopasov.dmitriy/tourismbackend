from ..models.rating_route import RatingRoute
from django.contrib import admin


@admin.register(RatingRoute)
class RatingRouteAdmin(admin.ModelAdmin):
    list_display = ['id', 'route', 'avg_rating', 'avg_rating_org', 'avg_rating_imp', 'avg_rating_service',
                    'avg_rating_price', 'number_of_ratings']
    list_editable = []
    search_fields = ['id', 'route__name']
    list_filter = []
    list_display_links = None
