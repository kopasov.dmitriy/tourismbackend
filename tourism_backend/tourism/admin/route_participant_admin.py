from ..models.route_participant import RouteParticipant
from django.contrib import admin


@admin.register(RouteParticipant)
class RouteParticipantAdmin(admin.ModelAdmin):
    list_display = ['id', 'route', 'user', 'reserved_places', 'message', 'payment', 'confirmed']
    list_editable = ['reserved_places', 'message']
    search_fields = ['id', 'route__name', 'user__email']
    list_filter = ['confirmed']
    list_display_links = None
