from django.contrib import admin
from ..models.user_confirmation_request import UserConfirmationRequest


@admin.register(UserConfirmationRequest)
class UserConfirmationRequestAdmin(admin.ModelAdmin):
    list_display = ['id', 'user', 'key', 'type', 'created']
    list_editable = ['key']
    search_fields = ['id', 'user__email', 'user__first_name', 'user__surname', 'key']
    list_filter = ['type']
    list_display_links = None
