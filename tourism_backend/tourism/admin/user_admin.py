from ..models.user import User
from django.contrib import admin


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ['id', 'email', 'first_name', 'surname', 'additional_name', 'country', 'region', 'city',
                    'image_file', 'phone_number', 'date_of_birthday', 'verified', 'created', 'modified']
    list_editable = ['email', 'first_name', 'surname', 'additional_name', 'country', 'region', 'city',
                     'image_file', 'phone_number', 'date_of_birthday']
    search_fields = ['id', 'email', 'first_name', 'surname', 'additional_name', 'phone_number', 'date_of_birthday']
    list_filter = ['verified']
    list_display_links = None
