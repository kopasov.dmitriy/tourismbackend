from ..models.user_role import UserRole
from django.contrib import admin


@admin.register(UserRole)
class UserRoleAdmin(admin.ModelAdmin):
    list_display = ['id', 'user', 'role']
    list_editable = []
    search_fields = ['id', 'user__email', 'user__first_name', 'user__surname']
    list_filter = ['role']
    list_display_links = None
