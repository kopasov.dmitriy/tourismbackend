from ..models.place import Place
from django.contrib import admin


@admin.register(Place)
class PlaceAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'short_description', 'description', 'location_latitude', 'location_longitude',
                    'country', 'region', 'city', 'image_file', 'created', 'created_by', 'modified']
    list_editable = ['name', 'short_description', 'description', 'location_latitude', 'location_longitude',
                     'country', 'region', 'city', 'image_file']
    search_fields = ['id', 'name', 'short_description', 'description']
    list_filter = ['region']
    list_display_links = None
