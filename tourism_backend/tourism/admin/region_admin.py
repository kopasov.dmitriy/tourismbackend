from ..models.region import Region
from django.contrib import admin


@admin.register(Region)
class RegionAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'image', 'latitude', 'longitude']
    list_editable = ['name', 'image', 'latitude', 'longitude']
    search_fields = ['id', 'name']
    list_filter = []
    list_display_links = None
