from ..common_functions import user_functions
from ..common_functions.auth_functions import hash_password
from ..common_functions.organizer_functions import get_current_organizer_or_none
from ..encrypter import encrypter
from ..exceptions.exceptions import CommonException
from ..google_drive.init_google_drive import CONFIDENTIAL_FOLDER
from ..models.organizer import Organizer
from ..models.role import Role
from ..models.user_role import UserRole
from ..models.user_uploaded_file import UserUploadedFile
from ..services.file_service import upload_file_common
import re


def get_organizer_info(organizer_id):
    organizer = Organizer.objects.filter(id=organizer_id).first()
    if organizer is None:
        raise CommonException(f'Организатор с идентификатором {organizer_id} не найден')
    return organizer


def create_organization(user, name, passport_series, passport_number, passport_issue_date, passport_issue_authority,
                        individual_tax_number):
    organizer = get_current_organizer_or_none(user)
    if organizer is not None:
        if organizer.verified:
            raise CommonException('Пользователь уже является организатором')
        else:
            raise CommonException('Пользователь уже подал заявку, чтобы стать организатором')
    if name is None:
        raise CommonException('Не указано название организатора')
    if passport_series is None:
        raise CommonException('Не указана серия паспорта')
    if len(passport_series) != 4 or re.match(r'\d', passport_series) is None:
        raise CommonException('Серия паспорта имеет неправильный формат')
    passport_series_enc = encrypter.encrypt_bytes(user.email, passport_series.encode()).decode()
    if passport_number is None:
        raise CommonException('Не указан номер паспорта')
    if len(passport_number) != 6 or re.match(r'\d', passport_number) is None:
        raise CommonException('Номер паспорта имеет неправильный формат')
    passport_number_enc = encrypter.encrypt_bytes(user.email, passport_number.encode()).decode()
    passport_series_number_hash = hash_password(passport_series + passport_number)
    if Organizer.objects.filter(passport_series_number_hash__icontains=passport_series_number_hash).first() is not None:
        raise CommonException('Пользователь с такими паспортными данными уже существует')
    if passport_issue_date is None:
        raise CommonException('Не указана дата выдачи паспорта')
    user_functions.convert_date(passport_issue_date)
    passport_issue_date_enc = encrypter.encrypt_bytes(user.email, passport_issue_date.encode()).decode()
    if passport_issue_authority is None:
        raise CommonException('Не указан орган выдачи паспорта')
    passport_issue_authority_enc = encrypter.encrypt_bytes(user.email, passport_issue_authority.encode()).decode()
    if individual_tax_number is None:
        raise CommonException('Не указан ИНН')
    if len(individual_tax_number) != 10 or re.match(r'\d', individual_tax_number) is None:
        raise CommonException('ИНН имеет неправильный формат')
    individual_tax_number_enc = encrypter.encrypt_bytes(user.email, individual_tax_number.encode()).decode()
    individual_tax_number_hash = hash_password(individual_tax_number)
    if Organizer.objects.filter(individual_tax_number_hash__icontains=individual_tax_number_hash).first() is not None:
        raise CommonException('Пользователь с такими ИНН уже существует')
    organizer = Organizer(name=name, user=user, passport_series_enc=passport_series_enc,
                          passport_number_enc=passport_number_enc,
                          passport_series_number_hash=passport_series_number_hash,
                          passport_issue_date_enc=passport_issue_date_enc,
                          passport_issue_authority_enc=passport_issue_authority_enc,
                          individual_tax_number_enc=individual_tax_number_enc,
                          individual_tax_number_hash=individual_tax_number_hash)
    organizer.save()
    return organizer


def attach_document(organizer, request, document_type):
    if document_type is None:
        raise CommonException('Не указан тип прикрепляемого документа')
    if document_type != 'passport' and document_type != 'itn':
        raise CommonException('Допустимый тип прикрепляемого документа должен быть '
                              'либо "passport" (паспорт), либо "itn" (ИНН)')
    if organizer.passport_original_file is not None and document_type == 'passport':
        raise CommonException('Оригинал паспорта уже был прикреплён')
    if organizer.individual_tax_number_original_file is not None and document_type == 'itn':
        raise CommonException('Оригинал ИНН уже был прикреплён')
    file_type = '????'
    if document_type == 'passport':
        file_type = 'PASSPORT'
    if document_type == 'itn':
        file_type = 'ITN'
    file = upload_file_common(request, CONFIDENTIAL_FOLDER, True, file_type)
    if document_type == 'passport':
        organizer.passport_original_file = file
    if document_type == 'itn':
        organizer.individual_tax_number_original_file = file
    organizer.save()
    validate_organizer(organizer)
    return organizer


def validate_organizer(organizer):
    if organizer.individual_tax_number_original_file is not None and organizer.passport_original_file is not None:
        organizer.verified = True
        organizer.save()
    role_organizer = Role.objects.filter(name='Организатор').first()
    if len(UserRole.objects.filter(role=role_organizer, user=organizer.user)) == 0:
        user_role_organizer = UserRole(role=role_organizer, user=organizer.user)
        user_role_organizer.save()


def find_organizers(name, verified, offset, limit):
    organizers = Organizer.objects.all().order_by('name')
    if name is not None:
        organizers = organizers.filter(name__icontains=name)
    if verified is not None:
        organizers = organizers.filter(verified=verified)
    return organizers[offset:offset + limit], len(organizers)
