from ..api.common_request_functions import is_special_test_user
from ..common_functions import email_functions, user_functions, auth_functions
from ..exceptions.exceptions import CommonException
from ..models.user_confirmation_request import UserConfirmationRequest
from ..models.user_password import UserPassword
from datetime import timedelta
from django.conf import settings
from django.utils import timezone


def reset_password(email):
    user = user_functions.find_user_by_email(email, False)
    if user is None:
        raise CommonException('Пользователь не найден')
    user_confirmation_requests = UserConfirmationRequest.objects.filter(user=user, type='password')
    user_confirmation_requests.delete()

    key = auth_functions.generate_auth_token(60).lower()
    if is_special_test_user(email):
        key = 'special_test_key'
    else:
        while True:
            similar_keys = UserConfirmationRequest.objects.filter(key=key)
            if len(similar_keys) == 0:
                break
            else:
                key = auth_functions.generate_auth_token(60).lower()

    request = UserConfirmationRequest(user=user,
                                      key=key,
                                      type='password')

    with open(settings.BASE_DIR / 'tourism/email_patterns/reset_password.html', encoding="utf-8") as email_file:
        message = email_file.read()
    message = message.replace('{KEY}', request.key)
    email_functions.send_text_email_as_html('Восстановление пароля',
                                            message,
                                            [email],
                                            message)
    request.save()


def check_password_request(email, key):
    # user = user_functions.find_user_by_email(email)
    if key is None:
        raise CommonException('Не указан код')

    user_requests = UserConfirmationRequest.objects.filter(type='password', key=key)

    if len(user_requests) != 1:
        raise CommonException('Введён недействительный код')

    if timezone.now() > user_requests[0].created + timedelta(hours=6):
        user_requests.delete()
        raise CommonException('Данный код больше не действителен')


def create_new_password(key, password):
    check_password_request(None, key)
    user_requests = UserConfirmationRequest.objects.filter(type='password', key=key)
    user = user_requests[0].user
    user_requests.delete()

    old_passwords = UserPassword.objects.filter(user=user)
    old_passwords.delete()

    user_password = UserPassword(user=user,
                                 password_hash=auth_functions.hash_password(password))
    user_password.save()
