from ..api.common_request_functions import is_special_test_user
from ..common_functions import email_functions, auth_functions
from ..exceptions.exceptions import CommonException
from ..models.user_confirmation_request import UserConfirmationRequest
from datetime import timedelta
from django.conf import settings
from django.utils import timezone


def ask_user_email_confirmation(user):
    if user.verified:
        raise CommonException('Электронная почта уже успешно подтверждена')
    user_confirmation_requests = UserConfirmationRequest.objects.filter(user=user, type='email')
    user_confirmation_requests.delete()

    key = auth_functions.generate_auth_token(60).lower()
    if is_special_test_user(user.email):
        key = 'special_test_key'
    registration_request = UserConfirmationRequest(user=user,
                                                   key=key,
                                                   type='email')

    with open(settings.BASE_DIR / 'tourism/email_patterns/email_confirmation.html', encoding="utf-8") as email_file:
        message = email_file.read()
    message = message.replace('{CONFIRMATION_CODE}', registration_request.key)
    email_functions.send_text_email_as_html('Регистрация на <название приложения>',
                                            message,
                                            [user.email],
                                            message)
    registration_request.save()


def confirm_user_email(user, key):
    if user.verified:
        raise CommonException('Электронная почта уже успешно подтверждена')

    if key is None:
        raise CommonException('Не указан код')

    user_request = UserConfirmationRequest.objects.filter(user=user,
                                                          key=key.lower(),
                                                          type='email')
    if len(user_request) != 1:
        raise CommonException('Введён недействительный код')

    user_request = user_request[0]

    if timezone.now() > user_request.created + timedelta(hours=6):
        user_request.delete()
        raise CommonException('Данный код больше не действителен')
    user.verified = True
    user.save()

    user_request.delete()


def send_email(email):
    if email is None:
        raise CommonException('Не указан тестовый почтовый адрес')
    email_functions.send_text_email('TEST TEST', ('test '*10)[:-1], [email])
