from ..common_functions import region_functions, target_group_functions, tourism_type_functions,\
    difficulty_type_functions
from ..common_functions.route_functions import get_route_by_id, get_existing_route_by_id, validate_rating, rate
from ..common_functions.user_functions import convert_date
from ..exceptions.exceptions import CommonException
from ..google_drive.init_google_drive import ROUTES_PHOTO_FOLDER
from ..models.difficulty_type import DifficultyType
from ..models.favorite_route import FavoriteRoute
from ..models.organizer import Organizer
from ..models.rating_route import RatingRoute
from ..models.region import Region
from ..models.review_route import ReviewRoute
from ..models.route import Route
from ..models.route_participant import RouteParticipant
from ..models.route_tourism_type import RouteTourismType
from ..models.target_group import TargetGroup
from ..models.tourism_type import TourismType
from .file_service import get_file_from_request_body
from django.db.models import Count, Sum
from django.db.models.functions import Length


def get_regions(name):
    regions = Region.objects.all()
    if name is not None:
        regions = regions.filter(name__icontains=name)
    return regions


def get_target_groups():
    target_groups = TargetGroup.objects.all()
    return target_groups


def get_tourism_types(name):
    tourism_types = TourismType.objects.all()
    if name is not None:
        tourism_types = tourism_types.filter(name__icontains=name)
    return tourism_types


def get_difficulty_types():
    difficulty_types = DifficultyType.objects.all()
    return difficulty_types


def create_route(user_creator, name, description, location_latitude, location_longitude,
                 country, region_id, city, cost, distance, min_tourists, max_tourists,
                 image, target_group_id, difficulty_type_id, tourism_type_ids, start_date, end_date):
    if name is None:
        raise CommonException('Не указано название маршрута')
    if description is None:
        raise CommonException('Не указано описание маршрута')
    # if location_latitude is None or location_longitude is None:
    # raise CommonException('Не указаны координаты расположения маршрута')
    if cost is None:
        raise CommonException('Не указана стоимость маршрута')
    if min_tourists is None:
        min_tourists = 1
    if max_tourists is not None and min_tourists > max_tourists:
        min_tourists, max_tourists = max_tourists, min_tourists
    if min_tourists < 1:
        raise CommonException('Минимальное количество туристов - 1')
    if region_id is None:
        region = None
    else:
        region = region_functions.get_existing_region_by_id(region_id)
    if target_group_id is None:
        target_group = None
    else:
        target_group = target_group_functions.get_existing_target_group_by_id(target_group_id)
    if difficulty_type_id is None:
        difficulty_type = None
    else:
        difficulty_type = difficulty_type_functions.get_existing_difficulty_type_by_id(difficulty_type_id)
    if tourism_type_ids is None:
        tourism_types = []
    else:
        tourism_types = tourism_type_functions.get_existing_tourism_types_by_id_list(tourism_type_ids)
    if start_date is not None and end_date is not None:
        start_date_converted = convert_date(start_date)
        end_date_converted = convert_date(end_date)
        if start_date_converted > end_date_converted:
            raise CommonException('Дата начала маршрута должна быть меньше или равна дате окончания маршрута: '
                                  f'{start_date} <= {end_date}')
    elif start_date is None and end_date is not None:
        raise CommonException('Указана дата окончания, но не указана дата начала')
    elif start_date is not None and end_date is None:
        raise CommonException('Указана дата начала, но не указана дата окончания')
    else:
        start_date_converted, end_date_converted = None, None

    organizer = Organizer.objects.filter(user=user_creator).first()

    image_file = get_file_from_request_body(image[0], image[1], ROUTES_PHOTO_FOLDER, 'ROUTE_IMAGE')

    route = Route(name=name, description=description,
                  location_latitude=location_latitude, location_longitude=location_longitude,
                  country=country, region=region, city=city, cost=cost, distance=distance,
                  min_tourists=min_tourists, max_tourists=max_tourists, image_file=image_file,
                  organizer=organizer, target_group=target_group, difficulty_type=difficulty_type,
                  start_date=start_date_converted, end_date=end_date_converted, created_by=user_creator.email)
    route.save()

    for tourism_type in tourism_types:
        RouteTourismType(tourism_type=tourism_type, route=route).save()

    return route


def find_routes(name, org_ids, reg_ids, target_group_id, tourism_type_ids, difficulty_type_ids,
                cost_greater_than, cost_less_than, distance_greater_than, distance_less_than,
                start_date_greater_than, start_date_less_than, end_date_greater_than, end_date_less_than,
                offset, limit):
    routes = Route.objects.all()
    if name is not None:
        routes = routes.filter(name__icontains=name)
    if org_ids is not None and len(org_ids) > 0:
        routes = routes.filter(organizer__id__in=org_ids)
    if reg_ids is not None and len(reg_ids) > 0:
        routes = routes.filter(region__id__in=reg_ids)
    if target_group_id is not None:
        routes = routes.filter(target_group__id=target_group_id)
    if tourism_type_ids is not None and len(tourism_type_ids) > 0:
        route_ids = [route_tourism_type.route.id for route_tourism_type in
                     RouteTourismType.objects.filter(tourism_type__id__in=tourism_type_ids)]
        routes = routes.filter(id__in=route_ids)
    if difficulty_type_ids is not None and len(difficulty_type_ids) > 0:
        routes = routes.filter(difficulty_type__id__in=difficulty_type_ids)
    if cost_greater_than is not None:
        routes = routes.filter(cost__gte=cost_greater_than)
    if cost_less_than is not None:
        routes = routes.filter(cost__lte=cost_less_than)
    if distance_greater_than is not None:
        routes = routes.filter(distance__gte=distance_greater_than)
    if distance_less_than is not None:
        routes = routes.filter(distance__lte=distance_less_than)
    if start_date_greater_than is not None:
        start_date_greater_than = convert_date(start_date_greater_than)
        routes = routes.filter(start_date__gte=start_date_greater_than)
    if start_date_less_than is not None:
        start_date_less_than = convert_date(start_date_less_than)
        routes = routes.filter(start_date__lte=start_date_less_than)
    if end_date_greater_than is not None:
        end_date_greater_than = convert_date(end_date_greater_than)
        routes = routes.filter(end_date__gte=end_date_greater_than)
    if end_date_less_than is not None:
        end_date_less_than = convert_date(end_date_less_than)
        routes = routes.filter(end_date__lte=end_date_less_than)
    return routes[offset:offset + limit], len(routes)


def find_routes_on_map(latitude_center, longitude_center,
                       latitude_delta, longitude_delta,
                       name, exclude_ids):
    routes = Route.objects.filter(location_latitude__isnull=False) \
        .filter(location_longitude__isnull=False)
    latitude_min = latitude_center - latitude_delta * 0.5
    latitude_max = latitude_center + latitude_delta * 0.5
    longitude_min = longitude_center - longitude_delta * 0.5
    if longitude_min < -180:
        longitude_min += 180
    longitude_max = longitude_center + longitude_delta * 0.5
    if longitude_max > 180:
        longitude_max -= 180
    result = []
    if longitude_delta > 20:
        regions = Region.objects.filter(latitude__gt=latitude_min) \
            .filter(latitude__lt=latitude_max) \
            .filter(longitude__gt=longitude_min) \
            .filter(longitude__lt=longitude_max)
        region_ids = regions.values_list('id', flat=True)
        routes = routes.filter(region__isnull=False).filter(region__id__in=region_ids)
        routes_in_region = routes.values('region').annotate(routes_count=Count('region')).order_by()
        for region_id in region_ids:
            region = regions.get(id=region_id)
            count = routes_in_region.filter(region=region_id)
            count = count[0] if len(count) == 1 else None
            region.count = count['routes_count'] if count is not None else 0
            result.append(region)
    else:
        if name is not None:
            routes = routes.filter(name__icontains=name)
        if exclude_ids is not None:
            routes = routes.exclude(id__in=exclude_ids)
        routes = routes.filter(location_latitude__gt=latitude_min)
        routes = routes.filter(location_latitude__lt=latitude_max)
        routes = routes.filter(location_longitude__gt=longitude_min)
        routes = routes.filter(location_longitude__lt=longitude_max)
        result = routes
    return result


def add_route_to_favorite(user, route_id):
    favorite_routes = get_favorite_routes(user, route_id)
    if len(favorite_routes) != 0:
        raise CommonException('Маршрут уже добавлен в избранное')
    favorite_route = FavoriteRoute(route=get_route_by_id(route_id), user=user)
    favorite_route.save()


def delete_route_from_favorite(user, route_id):
    favorite_routes = get_favorite_routes(user, route_id)
    if len(favorite_routes) == 0:
        raise CommonException('Маршрута нет в избранном')
    favorite_routes.delete()


def get_favorite_routes(user, route_id):
    route = get_existing_route_by_id(route_id)
    return FavoriteRoute.objects.filter(user=user, route=route)


def get_rating_for_route(route_id):
    route = get_existing_route_by_id(route_id)
    rating_route = RatingRoute.objects.filter(route=route).first()
    if rating_route is not None:
        return rating_route
    rating_route = RatingRoute(route=route, avg_rating=0, avg_rating_org=0, avg_rating_imp=0,
                               avg_rating_service=0, avg_rating_price=0, number_of_ratings=0)
    rating_route.save()
    return rating_route


def rate_route(user, route_id, rating_org, rating_imp, rating_service, rating_price, review_text):
    route = get_existing_route_by_id(route_id)
    previous_review = ReviewRoute.objects.filter(user=user, route=route).first()
    if previous_review is not None:
        raise CommonException('Пользователь уже оценил данный маршрут')

    validate_rating(rating_org, rating_imp, rating_service, rating_price)
    review = ReviewRoute(user=user, route=route, rating=(rating_org + rating_imp + rating_service + rating_price) / 4,
                         rating_org=rating_org, rating_imp=rating_imp,
                         rating_service=rating_service, rating_price=rating_price, review_text=review_text)
    review.save()
    rating_route = get_rating_for_route(route_id)
    rate(rating_route, rating_org, rating_imp, rating_service, rating_price)
    rating_route.save()


def get_route_reviews(route_id, offset, limit):
    route = get_existing_route_by_id(route_id)
    reviews = ReviewRoute.objects.filter(route=route).annotate(text_len=Length('review_text')).filter(text_len__gt=0) \
        .order_by('-created')
    return reviews[offset:offset + limit], len(reviews)


def get_sum_of_reserved_places(route):
    total_reserved_places = RouteParticipant.objects.filter(route=route, confirmed=True) \
        .aggregate(Sum('reserved_places'))['reserved_places__sum']
    if total_reserved_places is None:
        total_reserved_places = 0
    return total_reserved_places


def get_sum_of_payments(route):
    total_payments = RouteParticipant.objects.filter(route=route, confirmed=True) \
        .aggregate(Sum('payment'))['payment__sum']
    if total_payments is None:
        total_payments = 0
    return total_payments


def validate_route_reserved_places(route, reserved_places):
    if reserved_places is None:
        raise CommonException('Не указано количество резервируемых мест')
    if reserved_places < 1:
        raise CommonException('Количество резервируемых мест должно быть положительным целым числом')
    if route.max_tourists is not None:
        total_reserved_places = get_sum_of_reserved_places(route)
        if total_reserved_places + reserved_places > route.max_tourists:
            raise CommonException('Недостаточно свободных мест, для участия в этом маршруте. '
                                  f'Свободно мест: {route.max_tourists - total_reserved_places}. '
                                  f'Попытка зарезервировать мест: {reserved_places}.')


def participate(route_id, user, reserved_places, message):
    route = get_existing_route_by_id(route_id)
    current_participant = RouteParticipant.objects.filter(route=route, user=user).first()
    if current_participant is not None:
        # Нужно продумать возможность дополнить ранее оформленную заявку
        raise CommonException('Заявка на участие в данном маршруте ранее уже была подана')
    validate_route_reserved_places(route, reserved_places)
    RouteParticipant(user=user, route=route, reserved_places=reserved_places, message=message).save()


def get_participants(route_id, organizer, confirmed, offset, limit):
    route = get_existing_route_by_id(route_id)
    if route.organizer != organizer:
        raise CommonException('Вы не имеете доступ к участникам данного маршрута')
    route_participants = RouteParticipant.objects.filter(route=route)
    if confirmed is not None:
        route_participants = route_participants.filter(confirmed=confirmed)
    return route_participants[offset:offset + limit], len(route_participants)


def confirm_participation(route_participant_id, organizer, confirmed_places):
    if route_participant_id is None:
        raise CommonException('Не указан идентификатор участника маршрута')
    route_participant = RouteParticipant.objects.filter(id=route_participant_id).first()
    if route_participant is None:
        raise CommonException(f'Участник маршрута с id {route_participant_id} не найден')
    if route_participant.route.organizer != organizer:
        raise CommonException('Вы не имеете доступ к участникам данного маршрута')
    if route_participant.confirmed:
        raise CommonException('Участие данного участника уже подтверждено')
    if confirmed_places is None:
        confirmed_places = route_participant.reserved_places
    validate_route_reserved_places(route_participant.route, confirmed_places)
    if confirmed_places != route_participant.reserved_places:
        route_participant.reserved_places = confirmed_places
    if route_participant.route.cost != 0:
        route_participant.payment = route_participant.route.cost * confirmed_places
    route_participant.confirmed = True
    route_participant.save()

    return route_participant


def cancel_participation(route_participant_id, organizer):
    if route_participant_id is None:
        raise CommonException('Не указан идентификатор участника маршрута')
    route_participant = RouteParticipant.objects.filter(id=route_participant_id).first()
    if route_participant is None:
        raise CommonException(f'Участник маршрута с id {route_participant_id} не найден')
    if route_participant.route.organizer != organizer:
        raise CommonException('Вы не имеете доступ к участникам данного маршрута')
    if route_participant.confirmed:
        raise CommonException('Участие данного участника уже подтверждено')
    route_participant.delete()
