from ..common_functions import region_functions
from ..common_functions.place_functions import get_existing_place_by_id, get_place_by_id
from ..common_functions.route_functions import validate_rating, rate
from ..exceptions.exceptions import CommonException
from ..google_drive.init_google_drive import PLACES_PHOTO_FOLDER
from ..models.favorite_place import FavoritePlace
from ..models.place import Place
from ..models.rating_place import RatingPlace
from ..models.review_place import ReviewPlace
from .file_service import get_file_from_request_body
from django.db.models.functions import Length


def create_place(name, short_description, description, location_latitude, location_longitude,
                 country, region_id, city, image, creator_email):
    if name is None:
        raise CommonException('Не указано название места')
    if short_description is None:
        raise CommonException('Не указано краткое описание места')
    if description is None:
        raise CommonException('Не указано описание места')
    # if location_latitude is None or location_longitude is None:
    # raise CommonException('Не указаны координаты расположения места')
    if region_id is None:
        region = None
    else:
        region = region_functions.get_existing_region_by_id(region_id)

    image_file = get_file_from_request_body(image[0], image[1], PLACES_PHOTO_FOLDER, 'PLACE_IMAGE')

    place = Place(name=name, short_description=short_description, description=description,
                  location_latitude=location_latitude, location_longitude=location_longitude,
                  country=country, region=region, city=city, image_file=image_file, created_by=creator_email)
    place.save()

    return place


def find_places(name, offset, limit):
    places = Place.objects.all()
    if name is not None:
        places = places.filter(name__icontains=name)
    return places[offset:offset + limit], len(places)


def add_place_to_favorite(user, place_id):
    favorite_places = get_favorite_places(user, place_id)
    if len(favorite_places) != 0:
        raise CommonException('Место уже добавлено в избранное')
    favorite_place = FavoritePlace(place=get_place_by_id(place_id), user=user)
    favorite_place.save()


def delete_place_from_favorite(user, place_id):
    favorite_places = get_favorite_places(user, place_id)
    if len(favorite_places) == 0:
        raise CommonException('Места нет в избранном')
    favorite_places.delete()


def get_favorite_places(user, place_id):
    place = get_existing_place_by_id(place_id)
    return FavoritePlace.objects.filter(user=user, place=place)


def get_random_place():
    return Place.objects.order_by('?')[0]


def get_rating_for_place(place_id):
    place = get_existing_place_by_id(place_id)
    rating_place = RatingPlace.objects.filter(place=place).first()
    if rating_place is not None:
        return rating_place
    rating_place = RatingPlace(place=place, avg_rating=0, avg_rating_org=0, avg_rating_imp=0,
                               avg_rating_service=0, avg_rating_price=0, number_of_ratings=0)
    rating_place.save()
    return rating_place


def rate_place(user, place_id, rating_org, rating_imp, rating_service, rating_price, review_text):
    place = get_existing_place_by_id(place_id)
    previous_review = ReviewPlace.objects.filter(user=user, place=place).first()
    if previous_review is not None:
        raise CommonException('Пользователь уже оценил данное место')

    validate_rating(rating_org, rating_imp, rating_service, rating_price)
    review = ReviewPlace(user=user, place=place, rating=(rating_org + rating_imp + rating_service + rating_price) / 4,
                         rating_org=rating_org, rating_imp=rating_imp,
                         rating_service=rating_service, rating_price=rating_price, review_text=review_text)
    review.save()
    rating_place = get_rating_for_place(place_id)
    rate(rating_place, rating_org, rating_imp, rating_service, rating_price)
    rating_place.save()


def get_place_reviews(place_id, offset, limit):
    place = get_existing_place_by_id(place_id)
    reviews = ReviewPlace.objects.filter(place=place).annotate(text_len=Length('review_text')).filter(text_len__gt=0)\
        .order_by('-created')
    return reviews[offset:offset + limit], len(reviews)
