from ..common_functions import region_functions, user_functions
from ..common_functions.google_drive_functions import get_google_drive_file_access_link
from ..exceptions.exceptions import CommonException
from ..google_drive.init_google_drive import PROFILE_PHOTO_FOLDER
from ..models.favorite_place import FavoritePlace
from ..models.favorite_route import FavoriteRoute
from ..models.role import Role
from ..models.user_role import UserRole
from ..models.user_token import UserToken
from ..services.file_service import upload_file_common


def get_available_roles():
    return Role.objects.exclude(name='Администратор')


def choose_role(user, role_id):
    """if user.role is not None:
            raise CommonException('Роль уже выбрана и её нельзя сменить')
        role = UserRoleClassifier.objects.filter(id=role_id)
        if len(role) != 1:
            raise CommonException('Неверный индентификатор роли')
        user.role = role[0]
        user.save()"""
    raise CommonException('Нужно переработать данный функционал')


def update_user_profile_image(user, file):
    image_file = upload_file_common(file, PROFILE_PHOTO_FOLDER, False, 'PROFILE_IMAGE')
    user.image_file = image_file
    user.save()

    return get_google_drive_file_access_link(image_file.file_id)


def update_user_info(user, first_name, surname, additional_name, phone_number,
                     country, region_id, city, date_of_birthday):
    if first_name is not None:
        user.first_name = first_name
    if surname is not None:
        user.surname = surname
    if additional_name is not None:
        user.additional_name = additional_name
    if phone_number is not None:
        user.phone_number = phone_number
    if country is not None:
        user.country = country
    if region_id is not None:
        region = region_functions.get_existing_region_by_id(region_id)
        user.region = region
    if city is not None:
        user.city = city
    if date_of_birthday is not None:
        date_of_birthday = user_functions.convert_date(date_of_birthday)
        user_functions.validate_date_of_birthday(date_of_birthday)
    user.save()
    return user


def get_permissions(user):
    roles = UserRole.objects.filter(user=user)
    if len(roles) == 0:
        raise CommonException('У пользователя нет роли')
    actions = list(filter(lambda r: r.startswith('can_'), roles[0].role.__dir__()))
    permissions = {}
    for role in roles:
        for action in actions:
            if action in permissions:
                if permissions[action]:
                    continue
                else:
                    if getattr(role.role, action):
                        permissions[action] = True
            else:
                permissions[action] = getattr(role.role, action)
    return permissions


def get_favorite_routes(user, offset, limit):
    routes = [favorite_route.route for favorite_route in
              FavoriteRoute.objects.filter(user=user).order_by('route__name')]
    return routes[offset:offset + limit], len(routes)


def get_favorite_places(user, offset, limit):
    places = [favorite_place.place for favorite_place in
              FavoritePlace.objects.filter(user=user).order_by('place__name')]
    return places[offset:offset + limit], len(places)


def get_user_tokens(user, offset, limit):
    tokens = UserToken.objects.filter(user=user).order_by('-last_active', 'device_name')
    return tokens[offset:offset + limit], len(tokens)


def delete_tokens(user, current_token):
    tokens = UserToken.objects.exclude(token=current_token).filter(user=user)
    deleted_tokens_number = len(tokens)
    tokens.delete()
    return deleted_tokens_number


def delete_token(user, token_id):
    if token_id is None:
        raise CommonException('Не указан идентификатор токена')
    user_token = UserToken.objects.filter(id=token_id).first()
    if user_token is None:
        raise CommonException(f'Токен с id {token_id} не найден')
    if user_token.user != user:
        raise CommonException('Токен не принадлежит текущему пользователю')
    user_token.delete()
