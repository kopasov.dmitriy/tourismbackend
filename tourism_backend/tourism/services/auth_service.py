from . import email_service
from ..common_functions import user_functions, auth_functions, region_functions
from ..exceptions.exceptions import CommonException
from ..models.user_password import UserPassword
from ..models.user_uploaded_file import UserUploadedFile


def register_user(email, phone_number, password,
                  first_name, surname, additional_name,
                  country, region_id, city, date_of_birthday,
                  device_name):
    existing_user = user_functions.find_user_by_email(email, False)
    if existing_user is not None:
        raise CommonException(f'Указанный при регистрации email {email} уже занят')

    # if phone_number is not None:
    # Нужна валидация номера телефона
    if password is None:
        raise CommonException('Не указан пароль')
    if first_name is None:
        raise CommonException('Не указано имя')
    if surname is None:
        raise CommonException('Не указана фамилия')
    if date_of_birthday is not None:
        date_of_birthday = user_functions.convert_date(date_of_birthday)
        user_functions.validate_date_of_birthday(date_of_birthday)
    if region_id is None:
        region = None
    else:
        region = region_functions.get_existing_region_by_id(region_id)

    user = auth_functions.create_user(email, phone_number, password, first_name, surname, additional_name,
                                      country, region, city, date_of_birthday)
    return auth_functions.auth(user, device_name)


def auth_with_google(google_token, device_name):
    google_user_info = auth_functions.get_google_user_info(google_token)
    email = google_user_info.get('email')
    email_verified = google_user_info.get('email_verified')

    existing_user = user_functions.find_user_by_email(email, False)

    if existing_user is None:
        first_name = google_user_info.get('given_name')
        surname = google_user_info.get('family_name')
        image_link = google_user_info.get('picture')

        user = auth_functions.create_user(email, None, None, first_name, surname, None,
                                          None, None, None, None)

        if image_link is not None:
            image_file = UserUploadedFile(file_id=image_link, type='EXTERNAL_PROFILE_IMAGE', created_by=user.email)
            image_file.save()
            user.image_file = image_file
        if email_verified is not None and email_verified and not user.verified:
            user.verified = True
        if not user.verified:
            email_service.ask_user_email_confirmation(user)
    else:
        user = existing_user
        if email_verified is not None and email_verified and not user.verified:
            user.verified = True

    user.save()
    return auth_functions.auth(user, device_name)


def login(email, password, device_name):
    if password is None:
        raise CommonException('Не указан пароль')

    user = user_functions.find_user_by_email(email, False)
    login_success = False
    if user is not None:
        password_hash = auth_functions.hash_password(password)
        users_password = UserPassword.objects.filter(user=user)
        if len(users_password) == 1 and users_password[0].password_hash == password_hash:
            login_success = True

    if not login_success:
        raise CommonException('Неправильный email или пароль')

    return auth_functions.auth(user, device_name)


def logout(users_token):
    if users_token is not None:
        users_token.delete()
