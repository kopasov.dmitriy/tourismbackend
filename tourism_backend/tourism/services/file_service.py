from ..api.common_request_functions import get_string, get_int
from ..common_functions import auth_functions, google_drive_functions
from ..encrypter import encrypter
from ..exceptions.exceptions import CommonException
from ..google_drive.init_google_drive import CONFIDENTIAL_FOLDER, CONFIDENTIAL_FOLDER_ID, TEST_PHOTO_FOLDER_ID, \
    TEST_PHOTO_FOLDER, ROUTES_PHOTO_FOLDER_ID, ROUTES_PHOTO_FOLDER, PLACES_PHOTO_FOLDER_ID, PLACES_PHOTO_FOLDER, \
    PROFILE_PHOTO_FOLDER_ID, PROFILE_PHOTO_FOLDER
from ..models.user_uploaded_file import UserUploadedFile
import base64
import requests


def get_confidential_file(user, file_id):
    if file_id is None:
        raise CommonException('Не указан идентификатор файла')
    file = UserUploadedFile.objects.filter(file_id=file_id).first()
    if file is None:
        raise CommonException(f'Файл с идентификатором {file_id} не найден')
    try:
        user_email = user.email
        if file.created_by != user_email:
            user_email = file.created_by
        file_content = requests.get(f'https://drive.google.com/uc?export=view&id={file_id}').content
        file_content_as_base64 = base64.b64encode(encrypter.decrypt_bytes(user_email, file_content)).decode()
    except (Exception,):
        raise CommonException(f'Файл с идентификатором {file_id} не найден')
    return file_content_as_base64


def upload_file_common(request, folder=None, is_confidential=False, file_type=None):
    user = auth_functions.get_current_user(request, True)
    encrypting_key = None
    if is_confidential:
        encrypting_key = user.email
    if folder is None:
        folder = get_string(request.POST, 'storage_folder')
    if file_type is None:
        file_type = get_string(request.POST, 'file_type', '????')
    folder_id = get_folder_id(folder)
    file_base64 = get_string(request.POST, 'file_base64')
    small_photo_size = get_int(request.POST, 'small_photo_size')
    if small_photo_size is None:
        small_photo_size = get_small_photo_prefer_size(folder)
    if len(request.FILES) == 0 and file_base64 is None:
        raise CommonException('Не прикреплён файл')

    if len(request.FILES) > 1:
        raise CommonException('Можно прикрепить только один файл')

    file_id, small_file_id = None, None
    if len(request.FILES) == 1:
        for file in request.FILES:
            file = request.FILES[file]
            file_id, small_file_id = google_drive_functions.upload_file(file_size=file.size,
                                                                        file_content_type=file.content_type,
                                                                        file_bytes=file.read(),
                                                                        folder=folder_id,
                                                                        encrypting_key=encrypting_key,
                                                                        small_photo_size=small_photo_size)
    if file_base64 is not None:
        content_type = get_string(request.POST, 'file_content_type')
        if content_type is None:
            raise CommonException('При отправке файла в формате base64 также необходимо указать тип'
                                  ' его содержимого (MIME TYPE) параметром file_content_type')
        file_id, small_file_id = google_drive_functions.upload_file(file_content_type=content_type,
                                                                    file_base64=file_base64,
                                                                    folder=folder_id,
                                                                    encrypting_key=encrypting_key,
                                                                    small_photo_size=small_photo_size)

    if file_id is None:
        raise CommonException('Не удалось загрузить файл')
    uploaded_file = UserUploadedFile(file_id=file_id, type=file_type, created_by=user.email)
    if small_file_id is not None:
        uploaded_file.small_file_id = small_file_id
    uploaded_file.save()

    return uploaded_file


def get_file_from_request_body(file_id, request, folder=None, file_type='????'):
    file_result = None
    if file_id is None:
        try:
            file_result = upload_file_common(request, folder, False, file_type)
        except CommonException:
            pass
    else:
        file_result = UserUploadedFile.objects.filter(file_id=file_id).first()
    return file_result


def get_folder_id(file_storage_folder):
    if file_storage_folder is None:
        raise CommonException('Не указан целевая каталог для сохранения файла на сервере (storage_folder)')
    if file_storage_folder == PROFILE_PHOTO_FOLDER:
        return PROFILE_PHOTO_FOLDER_ID
    elif file_storage_folder == PLACES_PHOTO_FOLDER:
        return PLACES_PHOTO_FOLDER_ID
    elif file_storage_folder == ROUTES_PHOTO_FOLDER:
        return ROUTES_PHOTO_FOLDER_ID
    elif file_storage_folder == TEST_PHOTO_FOLDER:
        return TEST_PHOTO_FOLDER_ID
    elif file_storage_folder == CONFIDENTIAL_FOLDER:
        return CONFIDENTIAL_FOLDER_ID
    raise CommonException(f'Указан не корректный целевой каталог для'
                          f' сохранения файла на сервере - {file_storage_folder}')


def get_small_photo_prefer_size(file_storage_folder):
    if file_storage_folder == PROFILE_PHOTO_FOLDER:
        return 200
    elif file_storage_folder == PLACES_PHOTO_FOLDER:
        return 300
    elif file_storage_folder == ROUTES_PHOTO_FOLDER:
        return 300


def delete_file(file_id, _type=None):
    if file_id is None:
        raise CommonException(f'Не указан идентификатор файла')
    file = UserUploadedFile.objects.filter(file_id=file_id).first()
    is_external_file = _type == 'EXTERNAL_PROFILE_IMAGE'
    if is_external_file:
        file = UserUploadedFile.objects.filter(file_id__icontains='://')\
            .filter(file_id__icontains='http').filter(file_id__icontains=file_id).first()
    if file is None:
        raise CommonException(f'Файл с id {file_id} не найден')
    if not is_external_file:
        google_drive_functions.delete_file(file_id)
        if file.small_file_id is not None:
            google_drive_functions.delete_file(file.small_file_id)
    file.delete()


def get_files_usage():
    result = []
    files = UserUploadedFile.objects.all()
    for file in files:
        usage_as_passport_original = len(file.passport_original_files.all())
        usage_as_itn_original = len(file.individual_tax_number_original_files.all())
        usage_as_place_images = len(file.place_images.all())
        usage_as_route_images = len(file.route_images.all())
        usage_as_profile_images = len(file.profile_images.all())
        total_usage = (usage_as_passport_original +
                       usage_as_itn_original +
                       usage_as_place_images +
                       usage_as_route_images +
                       usage_as_profile_images)
        result.append({
            'file_id': file.file_id,
            'type': file.type,
            'usage_as_passport_original': usage_as_passport_original,
            'usage_as_itn_original': usage_as_itn_original,
            'usage_as_place_images': usage_as_place_images,
            'usage_as_route_images': usage_as_route_images,
            'usage_as_profile_images': usage_as_profile_images,
            'total_usage': total_usage
        })
    result.sort(key=lambda f: f['total_usage'], reverse=True)
    return result
