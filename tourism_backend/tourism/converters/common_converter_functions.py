def date_to_string(date):
    if date is not None:
        date = str(date).split('-')
        return date[2] + '.' + date[1] + '.' + date[0]
    return None


def is_field_available_and_correct(model, field):
    if isinstance(model, dict):
        return field in model \
               and model[field] is not None \
               and (model[field] != 0 or type(model[field]) == bool) \
               and model[field] != '' \
               and model[field] != '0'
    return hasattr(model, field) \
        and getattr(model, field) is not None \
        and (getattr(model, field) != 0 or type(getattr(model, field)) == bool) \
        and getattr(model, field) != '' \
        and getattr(model, field) != '0'


def get_all_fields_of_model_as_tuple(model, model_fields, add_field_to_model, request):
    all_fields = []
    for field in model_fields:
        field_value = add_field_to_model(model, field, request, dict())
        if field_value is not None:
            all_fields.append((field, field_value))
    return tuple(all_fields)


def get_value_for_dict_model(dict_model, field, *_):
    value = None
    if is_field_available_and_correct(dict_model, field):
        value = dict_model[field]
    return value
