from ..common_functions.google_drive_functions import get_google_drive_file_access_link
from .common_converter_functions import is_field_available_and_correct

regions_all_fields = ['count', 'name', 'latitude', 'longitude', 'image']

regions_all_fields_description = [
    'Число маршрутов на карте',
    'Название региона',
    'Широта',
    'Долгота',
    'Изображение'
]


def add_field_to_region(model, field, *_):
    field_value = None
    if is_field_available_and_correct(model, field):
        if field in ['count', 'name']:
            field_value = getattr(model, field)
        elif field in ['latitude', 'longitude']:
            field_value = float(getattr(model, field))
        elif field == 'image':
            field_value = get_google_drive_file_access_link(model.image)
    return field_value
