from .common_converter_functions import is_field_available_and_correct

target_groups_all_fields = ['name', 'description']

target_groups_all_fields_description = [
    'Название целевой группы',
    'Описание'
]


def add_field_to_target_group(model, field, *_):
    field_value = None
    if is_field_available_and_correct(model, field):
        if field in ['name', 'description']:
            field_value = str(getattr(model, field))
    return field_value
