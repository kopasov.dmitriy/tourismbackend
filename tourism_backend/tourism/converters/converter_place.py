from ..common_functions.auth_functions import get_current_user
from ..common_functions.google_drive_functions import get_google_drive_file_access_link
from ..exceptions.exceptions import LocalBaseException
from ..models.favorite_place import FavoritePlace
from ..models.review_place import ReviewPlace
from ..services import place_service
from .common_converter_functions import is_field_available_and_correct

places_all_fields = ['name', 'short_description', 'description', 'location_latitude', 'location_longitude',
                     'country', 'region', 'city', 'image', 'small_image', 'is_favorite', 'rating', 'review']

places_all_fields_description = [
    'Название места',
    'Краткое описание места',
    'Описание места',
    'Широта',
    'Долгота',
    'Страна',
    'Регион (объект Регион)',
    'Город',
    'Изображение',
    'Изображение в низком разрешении',
    'Место находится в избранном (для текущего пользователя)',
    'Общий рейтинг (объект Рейтинг)',
    'Обзор места (для текущего пользователя) (объект Обзор)'
]


def add_field_to_place(model, field, request, *_):
    field_value = None
    try:
        if field == 'is_favorite':
            user = get_current_user(request)
            favorite_place = FavoritePlace.objects.filter(user=user, place=model).first()
            field_value = favorite_place is not None
        elif field == 'rating':
            return place_service.get_rating_for_place(model.id)
        elif field == 'review':
            user = get_current_user(request)
            return ReviewPlace.objects.filter(user=user, place=model).first()
        elif field in ['image', 'small_image']:
            if model.image_file is not None:
                if field == 'small_image' and model.image_file.small_file_id is not None:
                    file_id = model.image_file.small_file_id
                else:
                    file_id = model.image_file.file_id
                field_value = get_google_drive_file_access_link(file_id)
        elif is_field_available_and_correct(model, field):
            if field in ['name', 'short_description', 'description', 'country', 'city']:
                field_value = str(getattr(model, field))
            elif field == 'region':
                field_value = model.region
            elif field == 'image':
                field_value = get_google_drive_file_access_link(model.image_file.file_id)
            elif field in ['location_latitude', 'location_longitude']:
                field_value = float(getattr(model, field))
    except LocalBaseException:
        pass
    return field_value
