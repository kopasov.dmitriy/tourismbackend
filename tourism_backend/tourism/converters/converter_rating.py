rating_all_fields = ['avg_rating', 'avg_rating_org', 'avg_rating_imp', 'avg_rating_service', 'avg_rating_price']

rating_all_fields_description = [
    'Средняя общая оценка',
    'Средняя оценка организации',
    'Средняя оценка от впечатлений',
    'Средняя оценка сервиса',
    'Средняя оценка цены'
]


def add_field_to_rating(model, field, *_):
    field_value = None
    if field in rating_all_fields:
        field_value = float(getattr(model, field))
    return field_value
