from ..api.common_request_functions import get_fields
from ..models.difficulty_type import DifficultyType
from ..models.organizer import Organizer
from ..models.place import Place
from ..models.rating_place import RatingPlace
from ..models.rating_route import RatingRoute
from ..models.region import Region
from ..models.review_place import ReviewPlace
from ..models.review_route import ReviewRoute
from ..models.role import Role
from ..models.route import Route
from ..models.route_participant import RouteParticipant
from ..models.target_group import TargetGroup
from ..models.tourism_type import TourismType
from ..models.user import User
from ..models.user_token import UserToken
from .common_converter_functions import get_all_fields_of_model_as_tuple, get_value_for_dict_model
from .converter_difficulty_type import add_field_to_difficulty_type, difficulty_types_all_fields
from .converter_organizer import add_field_to_organizer, organizers_all_fields
from .converter_place import add_field_to_place, places_all_fields
from .converter_rating import add_field_to_rating, rating_all_fields
from .converter_region import add_field_to_region, regions_all_fields
from .converter_review import add_field_to_review, review_all_fields
from .converter_role import add_field_to_role, roles_all_fields
from .converter_route import add_field_to_route, routes_all_fields
from .converter_route_participant import add_field_to_route_participant, route_participants_all_fields
from .converter_target_group import add_field_to_target_group, target_groups_all_fields
from .converter_tourism_type import add_field_to_tourism_type, tourism_types_all_fields
from .converter_user import add_field_to_user, users_all_fields
from .converter_user_token import add_field_to_user_token, user_tokens_all_fields
from django.db.models import Model


def _add_field_to_model_(model, field, request, extra_params):
    if field == 'all':
        return _get_all_fields_of_model_as_tuple_(model, request)
    else:
        field_value = None
        if isinstance(model, DifficultyType):
            field_value = add_field_to_difficulty_type(model, field, request, extra_params)
        elif isinstance(model, Organizer):
            field_value = add_field_to_organizer(model, field, request, extra_params)
        elif isinstance(model, Place):
            field_value = add_field_to_place(model, field, request, extra_params)
        elif isinstance(model, RatingRoute) or isinstance(model, RatingPlace):
            field_value = add_field_to_rating(model, field, request, extra_params)
        elif isinstance(model, Region):
            field_value = add_field_to_region(model, field, request, extra_params)
        elif isinstance(model, ReviewPlace) or isinstance(model, ReviewRoute):
            field_value = add_field_to_review(model, field, request, extra_params)
        elif isinstance(model, Role):
            field_value = add_field_to_role(model, field, request, extra_params)
        elif isinstance(model, Route):
            field_value = add_field_to_route(model, field, request, extra_params)
        elif isinstance(model, RouteParticipant):
            field_value = add_field_to_route_participant(model, field, request, extra_params)
        elif isinstance(model, TargetGroup):
            field_value = add_field_to_target_group(model, field, request, extra_params)
        elif isinstance(model, TourismType):
            field_value = add_field_to_tourism_type(model, field, request, extra_params)
        elif isinstance(model, User):
            field_value = add_field_to_user(model, field, request, extra_params)
        elif isinstance(model, UserToken):
            field_value = add_field_to_user_token(model, field, request, extra_params)
        elif isinstance(model, dict):
            field_value = get_value_for_dict_model(model, field)
        return field_value


def _get_all_fields_of_model_as_tuple_(model, request):
    all_fields = []
    add_fields_to_model = None
    if isinstance(model, DifficultyType):
        all_fields = difficulty_types_all_fields
        add_fields_to_model = add_field_to_difficulty_type
    elif isinstance(model, Organizer):
        all_fields = organizers_all_fields
        add_fields_to_model = add_field_to_organizer
    elif isinstance(model, Place):
        all_fields = places_all_fields
        add_fields_to_model = add_field_to_place
    elif isinstance(model, RatingRoute) or isinstance(model, RatingPlace):
        all_fields = rating_all_fields
        add_fields_to_model = add_field_to_rating
    elif isinstance(model, Region):
        all_fields = regions_all_fields
        add_fields_to_model = add_field_to_region
    elif isinstance(model, ReviewRoute) or isinstance(model, ReviewPlace):
        all_fields = review_all_fields
        add_fields_to_model = add_field_to_review
    elif isinstance(model, Role):
        all_fields = roles_all_fields
        add_fields_to_model = add_field_to_role
    elif isinstance(model, Route):
        all_fields = routes_all_fields
        add_fields_to_model = add_field_to_route
    elif isinstance(model, RouteParticipant):
        all_fields = route_participants_all_fields
        add_fields_to_model = add_field_to_route_participant
    elif isinstance(model, TargetGroup):
        all_fields = target_groups_all_fields
        add_fields_to_model = add_field_to_target_group
    elif isinstance(model, TourismType):
        all_fields = tourism_types_all_fields
        add_fields_to_model = add_field_to_tourism_type
    elif isinstance(model, User):
        all_fields = users_all_fields
        add_fields_to_model = add_field_to_user
    elif isinstance(model, UserToken):
        all_fields = user_tokens_all_fields
        add_fields_to_model = add_field_to_user_token
    elif isinstance(model, dict):
        all_fields = list(model)
        add_fields_to_model = get_value_for_dict_model
    if add_fields_to_model is None:
        return None
    else:
        return get_all_fields_of_model_as_tuple(model, all_fields, add_fields_to_model, request)


def extract_extra_params(field):
    extra_params = dict()
    if '{' in field and '}' in field:
        for key_value in field[field.find('{') + 1: field.find('}')].split('|'):
            try:
                key = key_value.split('=')[0]
                value = key_value.split('=')[1]
                if value.isdigit():
                    value = int(value)
                elif value.lower() in ['false', 'true']:
                    value = value == 'true'
                elif value.replace('.', '', 1).isdigit():
                    value = float(value)
                extra_params[key] = value
            except (Exception,):
                pass
        field = field[0: field.find('{')]
    return extra_params, field


def convert(model, request, fields=None, all_nested_fields=None):
    if fields is None:
        fields = get_fields(request)
    if model is None:
        return {}
    nested_fields = []
    if all_nested_fields:
        nested_fields = all_nested_fields
    else:
        while ':' in fields:
            nested_param_start_position = fields.rfind(':(')
            nested_param_end_position = fields.find(')', nested_param_start_position)
            if nested_param_start_position == -1 or nested_param_end_position == -1:
                fields = fields.replace(':', '', 1)
                continue
            nested_field = fields[nested_param_start_position: nested_param_end_position + 1]
            nested_field_replacer = f'${len(nested_fields)}'
            fields = fields.replace(nested_field, nested_field_replacer)
            nested_fields.append(nested_field.replace(':(', '').replace(')', ''))

    def _convert_(_model_, _fields_):
        converted = {}
        if hasattr(_model_, 'id'):
            converted['id'] = int(getattr(_model_, 'id'))

        for field in _fields_.split(','):
            try:
                nested_index = None
                if '$' in field:
                    nested_index = field.split('$')
                    field = nested_index[0]
                    nested_index = int(nested_index[1])
                extra_params, field = extract_extra_params(field)
                field_value = _add_field_to_model_(_model_, field, request, extra_params)
                if field_value is not None:
                    if isinstance(field_value, tuple):
                        for key, value in list(field_value):
                            if isinstance(value, Model):
                                converted[key] = {
                                    'id': int(value.id),
                                }
                            elif isinstance(value, dict):
                                converted[key] = {}
                            elif isinstance(value, list):
                                values = []
                                for _value in value:
                                    if isinstance(_value, Model):
                                        values.append({
                                            'id': int(_value.id),
                                        })
                                if len(values) > 0:
                                    converted[key] = values
                            else:
                                converted[key] = value
                    else:
                        if nested_index is None:
                            if isinstance(field_value, Model):
                                converted[field] = {
                                    'id': int(field_value.id),
                                }
                            elif isinstance(field_value, dict) and len(field_value) == 0:
                                converted[field] = {}
                            elif isinstance(field_value, list) and field != 'all':
                                values = []
                                for value in field_value:
                                    if isinstance(value, Model):
                                        values.append({
                                            'id': int(value.id),
                                        })
                                if len(values) > 0:
                                    converted[field] = values
                            else:
                                converted[field] = field_value
                        else:
                            if isinstance(field_value, list):
                                if len(field_value) > 0:
                                    converted[field] = convert_list(
                                        field_value,
                                        request,
                                        nested_fields[nested_index],
                                        nested_fields
                                    )
                            else:
                                converted[field] = _convert_(field_value, nested_fields[nested_index])
            except (Exception,):
                continue
        return converted
    return _convert_(model, fields)


def convert_list(models, request, fields=None, all_nested_fields=None):
    converted_list = []
    if models is not None:
        for model in models:
            converted_list.append(convert(model, request, fields, all_nested_fields))
    return converted_list
