from ..exceptions.exceptions import LocalBaseException
from .common_converter_functions import is_field_available_and_correct

route_participants_all_fields = ['route', 'user', 'reserved_places', 'message', 'payment', 'confirmed']

route_participants_all_fields_description = [
    'Маршрут (объект Маршрут)',
    'Пользователь - участник маршрута (объект Пользователь)',
    'Зарезервировано мест для участника в данном маршруте',
    'Сообщение в заявке',
    'Сумма оплаченного заказа',
    'Участие в маршруте подтверждено'
]


def add_field_to_route_participant(model, field, *_):
    field_value = None
    try:
        if is_field_available_and_correct(model, field):
            if field in ['route', 'user', 'reserved_places', 'message', 'payment', 'confirmed']:
                field_value = getattr(model, field)
    except LocalBaseException:
        pass
    return field_value
