from ..common_functions.google_drive_functions import get_google_drive_file_access_link
from ..models.user_role import UserRole
from .common_converter_functions import date_to_string, is_field_available_and_correct

users_all_fields = ['email', 'first_name', 'surname', 'additional_name', 'country', 'region', 'city', 'phone_number',
                    'image', 'image_small', 'date_of_birthday', 'role']

users_all_fields_description = [
    'Электронный адрес пользователя',
    'Имя',
    'Фамилия',
    'Отчество (или дополнительное имя)',
    'Страна',
    'Регион (объект Регион)',
    'Город',
    'Номер телефона',
    'Изображение',
    'Изображение в низком разрешении',
    'Дата рождения',
    'Список ролей на сайте (объект Роль)'
]


def add_field_to_user(model, field, *_):
    field_value = None
    if field == 'role':
        roles = UserRole.objects.filter(user__id=model.id)
        if len(roles) != 0:
            field_value = ', '.join([role.role.name for role in roles])
    elif field in ['image', 'small_image']:
        if model.image_file is not None:
            if field == 'small_image' and model.image_file.small_file_id is not None:
                file_id = model.image_file.small_file_id
            else:
                file_id = model.image_file.file_id
            field_value = get_google_drive_file_access_link(file_id)
    elif is_field_available_and_correct(model, field):
        if field == 'date_of_birthday':
            field_value = date_to_string(model.date_of_birthday)
        elif field in ['email', 'first_name', 'surname', 'additional_name', 'country', 'city', 'phone_number']:
            if is_field_available_and_correct(model, field):
                field_value = str(getattr(model, field))
        elif field == 'region':
            if is_field_available_and_correct(model, field):
                field_value = model.region
    return field_value
