from .common_converter_functions import is_field_available_and_correct

user_tokens_all_fields = ['token', 'device_name', 'last_active']

user_tokens_all_fields_description = [
    'Токен доступа',
    'Устройство, с которого производилась авторизация',
    'Дата последней активности'
]


def add_field_to_user_token(model, field, *_):
    field_value = None
    if is_field_available_and_correct(model, field):
        if field == 'token':
            field_value = model.token
        elif field == 'device_name':
            field_value = model.device_name
        elif field == 'last_active':
            field_value = model.last_active
    return field_value
