from ..common_functions.auth_functions import get_current_user
from ..common_functions.user_functions import check_user_can_do_action
from ..encrypter import encrypter
from ..exceptions.exceptions import LocalBaseException
from ..services.file_service import get_confidential_file
from .common_converter_functions import is_field_available_and_correct

organizers_all_fields = ['name', 'user', 'passport_series', 'passport_number', 'passport_issue_date',
                         'passport_issue_authority', 'passport_original_file', 'is_passport_original_file_attached',
                         'individual_tax_number', 'individual_tax_number_original_file',
                         'is_individual_tax_number_original_file_attached', 'verified']

organizers_all_fields_description = [
    'Название организации',
    'Пользователь - владелец организации (объект Пользователь)',
    'Серия паспорта (доступно только для самого организатора или администратора)',
    'Номер паспорта (доступно только для самого организатора или администратора)',
    'Дата выдачи паспорта (доступно только для самого организатора или администратора)',
    'Орган выдавший паспорт (доступно только для самого организатора или администратора)',
    'Файл оригинала паспорта в формате Base64 (доступно только для самого организатора или администратора)',
    'Прикреплён ли оригинал паспорта (доступно только для самого организатора или администратора)',
    'ИНН (доступно только для самого организатора или администратора)',
    'Файл оригинала ИНН в формате Base64 (доступно только для самого организатора или администратора)',
    'Прикреплён ли оригинал ИНН (доступно только для самого организатора или администратора)',
    'Подтверждён ли организатор'
]


def __can_view_confidential_data(user, model):
    can_view_confidential_data = model.user == user
    if not can_view_confidential_data:
        can_view_confidential_data = check_user_can_do_action(user, 'can_view_confidential_data', False)
    return can_view_confidential_data


def add_field_to_organizer(model, field, request, *_):
    field_value = None
    try:
        if field in ['passport_series', 'passport_number', 'passport_issue_date', 'passport_issue_authority',
                     'individual_tax_number']:
            user = get_current_user(request)
            can_view_confidential_data = __can_view_confidential_data(user, model)
            if can_view_confidential_data:
                field_value = encrypter.decrypt_bytes(user.email, str(getattr(model, field + '_enc')).encode()).decode()
        elif field in ['is_passport_original_file_attached', 'is_individual_tax_number_original_file_attached']:
            user = get_current_user(request)
            can_view_confidential_data = __can_view_confidential_data(user, model)
            if can_view_confidential_data:
                if field == 'is_passport_original_file_attached':
                    field_value = model.passport_original_file is not None
                elif field == 'is_individual_tax_number_original_file_attached':
                    field_value = model.individual_tax_number_original_file is not None
        elif is_field_available_and_correct(model, field):
            if field == 'name':
                field_value = model.name
            elif field == 'user':
                field_value = model.user
            elif field in ['passport_original_file', 'individual_tax_number_original_file']:
                user = get_current_user(request)
                can_view_confidential_data = __can_view_confidential_data(user, model)
                if can_view_confidential_data:
                    file = getattr(model, field)
                    if file is not None:
                        field_value = get_confidential_file(user, file.file_id)
            elif field == 'verified':
                field_value = model.verified
    except LocalBaseException:
        pass
    return field_value
