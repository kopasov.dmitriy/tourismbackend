from ..common_functions.google_drive_functions import get_google_drive_file_access_link
from .common_converter_functions import is_field_available_and_correct

roles_all_fields = ['name', 'description', 'image']

roles_all_fields_description = [
    'Название роли',
    'Описание',
    'Изображение'
]


def add_field_to_role(model, field, *_):
    field_value = None
    if is_field_available_and_correct(model, field):
        if field in ['name', 'description']:
            field_value = str(getattr(model, field))
        elif field == 'image':
            field_value = get_google_drive_file_access_link(model.image)
    return field_value
