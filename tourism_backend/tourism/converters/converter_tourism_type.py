from .common_converter_functions import is_field_available_and_correct

tourism_types_all_fields = ['name', 'description']

tourism_types_all_fields_description = [
    'Название типа туризма',
    'Описание'
]


def add_field_to_tourism_type(model, field, *_):
    field_value = None
    if is_field_available_and_correct(model, field):
        if field in ['name', 'description']:
            field_value = str(getattr(model, field))
    return field_value
