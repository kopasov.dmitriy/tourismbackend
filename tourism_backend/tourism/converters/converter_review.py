from ..common_functions.auth_functions import get_current_user
from ..exceptions.exceptions import CommonException

review_all_fields = ['user', 'rating', 'rating_org', 'rating_imp', 'rating_service', 'rating_price', 'review_text',
                     'created']

review_all_fields_description = [
    'Автор обзора (объект Пользователь)',
    'Средняя оценка',
    'Оценка организации',
    'Оценка впечатлений',
    'Оценка сервиса',
    'Оценка цены',
    'Текст обзора',
    'Дата создания'
]


def check_can_view_rating(request, model):
    can_view_rating = False
    try:
        user = get_current_user(request)
        if user == model.user:
            can_view_rating = True
    except CommonException:
        pass
    return can_view_rating


def add_field_to_review(model, field, request, *_):
    field_value = None
    if field == 'rating':
        can_view_rating = check_can_view_rating(request, model)
        if not can_view_rating and model.review_text is not None and len(model.review_text) > 0:
            can_view_rating = True
        if can_view_rating:
            field_value = model.rating
    elif field in ['rating_org', 'rating_imp', 'rating_service', 'rating_price']:
        can_view_rating = check_can_view_rating(request, model)
        if can_view_rating:
            field_value = getattr(model, field)
    elif field == 'review_text':
        field_value = model.review_text
    elif field == 'created':
        field_value = model.created
    elif field == 'user':
        field_value = model.user
    return field_value
