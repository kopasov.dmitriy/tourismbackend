from ..common_functions.auth_functions import get_current_user
from ..common_functions.google_drive_functions import get_google_drive_file_access_link
from ..exceptions.exceptions import LocalBaseException
from ..models.favorite_route import FavoriteRoute
from ..models.review_route import ReviewRoute
from ..models.route_participant import RouteParticipant
from ..models.route_tourism_type import RouteTourismType
from ..services import route_service
from ..services.route_service import get_sum_of_reserved_places, get_sum_of_payments, get_participants
from .common_converter_functions import is_field_available_and_correct, date_to_string

routes_all_fields = ['name', 'description', 'location_latitude', 'location_longitude', 'country', 'city', 'cost',
                     'distance', 'min_tourists', 'max_tourists', 'region', 'image', 'image_small', 'organizer',
                     'is_favorite', 'rating', 'review', 'target_group', 'difficulty_type', 'tourism_types',
                     'current_user_route_participant', 'places_left', 'payments',
                     'participants_confirmed', 'participants_confirmed_count',
                     'participants_requests', 'participants_requests_count',
                     'start_date', 'end_date']

routes_all_fields_description = [
    'Название маршрута',
    'Описание',
    'Широта',
    'Долгота',
    'Страна',
    'Город',
    'Стоимость',
    'Протяжённость',
    'Миниальное число туристов на маршруте',
    'Максимальное число туристов на маршруте',
    'Регион (объект Регион)',
    'Изображение',
    'Изображение в низком разрешении',
    'Организатор маршрута (объект Организатор)',
    'Маршрут находится в избранном (для текущего пользователя)',
    'Общий рейтинг (объект Рейтинг)',
    'Обзор маршрута (для текущего пользователя) (объект Обзор)',
    'Целевая группа, на которую ориентирован маршрут (объект Целевая группа)',
    'Уровень сложности (объект Уровень сложности)',
    'Список типов туризма (объект Тип туризма)',
    'Участие в маршруте текущего пользователя (для текущего пользователя) (объект Участник маршрута)',
    'Число оставшихся мест на маршруте',
    'Сумма всех оплаченных заказов на маршруте',
    'Список подтверждённых участников (только для организатора этого маршрута) (массив объектов Участник маршрута)',
    'Число подтверждённых участников (только для организатора этого маршрута)',
    'Список заявок на участие в маршруте (только для организатора этого маршрута) (массив объектов Участник маршрута)',
    'Число заявок на участие в маршруте (только для организатора этого маршрута)',
    'Дата начала проведения маршрута',
    'Дата окончания маршрута'
]


def add_field_to_route(model, field, request, extra_params):
    field_value = None
    try:
        if field == 'is_favorite':
            user = get_current_user(request)
            favorite_route = FavoriteRoute.objects.filter(user=user, route=model).first()
            field_value = favorite_route is not None
        elif field == 'current_user_route_participant':
            user = get_current_user(request)
            field_value = RouteParticipant.objects.filter(user=user, route=model).first()
        elif field == 'rating':
            return route_service.get_rating_for_route(model.id)
        elif field == 'review':
            user = get_current_user(request)
            return ReviewRoute.objects.filter(user=user, route=model).first()
        elif field == 'tourism_types':
            return [route_tourism_type.tourism_type for route_tourism_type in
                    RouteTourismType.objects.filter(route=model).order_by('tourism_type__name')]
        elif field == 'participants_requests_count':
            user = get_current_user(request)
            if user == model.organizer.user:
                field_value = len(RouteParticipant.objects.filter(route=model, confirmed=False))
        elif field in ['participants_confirmed', 'participants_requests']:
            user = get_current_user(request)
            if user == model.organizer.user:
                field_value, _ = get_participants(
                    model.id,
                    model.organizer,
                    field == 'participants_confirmed',
                    extra_params.get('offset', 0),
                    extra_params.get('limit', 30)
                )
                field_value = list(field_value)
        elif field == 'participants_confirmed_count':
            user = get_current_user(request)
            if user == model.organizer.user:
                field_value = get_sum_of_reserved_places(model)
        elif field == 'places_left':
            if model.max_tourists is None:
                field_value = -1
            else:
                field_value = model.max_tourists - get_sum_of_reserved_places(model)
        elif field == 'payments':
            field_value = get_sum_of_payments(model)
        elif field in ['image', 'small_image']:
            if model.image_file is not None:
                if field == 'small_image' and model.image_file.small_file_id is not None:
                    file_id = model.image_file.small_file_id
                else:
                    file_id = model.image_file.file_id
                field_value = get_google_drive_file_access_link(file_id)
        elif is_field_available_and_correct(model, field):
            if field in ['name', 'description', 'country', 'city']:
                field_value = str(getattr(model, field))
            elif field in ['location_latitude', 'location_longitude', 'cost', 'distance']:
                field_value = float(getattr(model, field))
            elif field in ['min_tourists', 'max_tourists']:
                field_value = int(getattr(model, field))
            elif field == 'region':
                field_value = model.region
            elif field == 'image':
                field_value = get_google_drive_file_access_link(model.image_file.file_id)
            elif field == 'organizer':
                field_value = model.organizer
            elif field == 'target_group':
                field_value = model.target_group
            elif field == 'difficulty_type':
                field_value = model.difficulty_type
            elif field in ['start_date', 'end_date']:
                field_value = date_to_string(getattr(model, field))
    except LocalBaseException:
        pass
    return field_value
