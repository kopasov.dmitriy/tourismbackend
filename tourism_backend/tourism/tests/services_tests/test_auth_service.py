from ...exceptions.exceptions import CommonException
from ...models import user, user_token, user_password, user_confirmation_request
from ...services import auth_service
from datetime import timedelta
from django.test import TestCase


class AuthServiceRegisterTests(TestCase):
    def setUp(self):
        self.user = None
        # self.addCleanup(self.deleteCreatedUser)

    def test_empty_email(self):
        with self.assertRaises(CommonException) as error:
            auth_service.register_user(None, None, None, None, None, None, None, None, None, None)
        exception = error.exception
        self.assertEqual(str(exception), 'Не указан email')
        self.assertEqual(exception.status, 400)

    def test_incorrect_email(self):
        with self.assertRaises(CommonException) as error:
            auth_service.register_user('aaaaa.aaaaa', None, None, None, None, None, None, None, None, None)
        exception = error.exception
        self.assertEqual(str(exception), 'Указанный email имеет неправильный формат')
        self.assertEqual(exception.status, 400)

    def test_existing_user(self):
        user = User(email='aaa@aaa.aaa', first_name='aaa', surname='aaa')
        user.save()
        with self.assertRaises(CommonException) as error:
            auth_service.register_user('aaa@aaa.aaa', None, None, None, None, None, None, None, None, None)
        exception = error.exception
        self.assertEqual(str(exception), 'Указанный при регистрации email aaa@aaa.aaa уже занят')
        self.assertEqual(exception.status, 400)

    def test_works_correct(self):
        self.user = auth_service.register_user('dvk48@tpu.ru', None, '0000', 'TEST 1', 'TEST 2',
                                               None, None, None, None, None).user
        self.assertEqual(self.user.email, 'dvk48@tpu.ru')
        self.assertEqual(self.user.first_name, 'TEST 1')
        self.assertEqual(self.user.surname, 'TEST 2')
        self.assertEqual(self.user.surname, 'TEST 2')
        self.assertEqual(self.user.additional_name, None)
        self.assertEqual(self.user.country, None)
        self.assertEqual(self.user.region, None)
        self.assertEqual(self.user.city, None)
        self.assertEqual(self.user.image, None)
        self.assertEqual(self.user.phone_number, None)
        self.assertEqual(self.user.role, None)
        self.assertEqual(self.user.date_of_birthday, None)

    def test_empty_password(self):
        with self.assertRaises(CommonException) as error:
            auth_service.register_user('dvk48@tpu.ru', None, None, None, None, None, None, None, None, None)
        exception = error.exception
        self.assertEqual(str(exception), 'Не указан пароль')
        self.assertEqual(exception.status, 400)

    def test_empty_first_name(self):
        with self.assertRaises(CommonException) as error:
            auth_service.register_user('dvk48@tpu.ru', None, '1111', None, None, None, None, None, None, None)
        exception = error.exception
        self.assertEqual(str(exception), 'Не указано имя')
        self.assertEqual(exception.status, 400)

    def test_empty_surname_name(self):
        with self.assertRaises(CommonException) as error:
            auth_service.register_user('dvk48@tpu.ru', None, '1111', 'aaaa', None, None, None, None, None, None)
        exception = error.exception
        self.assertEqual(str(exception), 'Не указана фамилия')
        self.assertEqual(exception.status, 400)

    def test_incorrect_date_of_birthday(self):
        with self.assertRaises(CommonException) as error:
            auth_service.register_user('dvk48@tpu.ru', None, '1111', 'aaaa', 'aaaa', None,
                                       None, None, None, '10.10.9999')
        exception = error.exception
        self.assertEqual(str(exception), 'Указанная дата рождения больше текущей даты, что недопустимо')
        self.assertEqual(exception.status, 400)

    '''def deleteCreatedUser(self):
        if self.user is not None:
            self.user.delete()'''


class AuthServiceLoginTests(TestCase):
    def setUp(self):
        user = User.objects.create(email='aaa@aaa.aaa', first_name='222', surname='333')
        UserPassword.objects.create(user=user, password_hash='25f9e794323b453885f5181f1b624d0b')

    def test_empty_email(self):
        with self.assertRaises(CommonException) as error:
            auth_service.login(None, None)
        exception = error.exception
        self.assertEqual(str(exception), 'Не указан email')
        self.assertEqual(exception.status, 400)

    def test_empty_password(self):
        with self.assertRaises(CommonException) as error:
            auth_service.login('aaa@aaa.aaa', None)
        exception = error.exception
        self.assertEqual(str(exception), 'Не указан пароль')
        self.assertEqual(exception.status, 400)

    def test_incorrect_email(self):
        with self.assertRaises(CommonException) as error:
            auth_service.login('aaaa@aaaa.aaaa', '123456789')
        exception = error.exception
        self.assertEqual(str(exception), 'Неправильный email или пароль')
        self.assertEqual(exception.status, 400)

    def test_incorrect_password(self):
        with self.assertRaises(CommonException) as error:
            auth_service.login('aaa@aaa.aaa', '0000')
        exception = error.exception
        self.assertEqual(str(exception), 'Неправильный email или пароль')
        self.assertEqual(exception.status, 400)

    def test_success_login(self):
        token = auth_service.login('aaa@aaa.aaa', '123456789')
        self.assertIsNotNone(token)
        self.assertEqual(len(token), 64)


class AuthServiceLogoutTests(TestCase):
    def test_empty_token_works(self):
        result = auth_service.logout(None)
        self.assertIsNone(result)

    def test_works_correct(self):
        user = User.objects.create(email='aaa@aaa.aaa', first_name='222', surname='333')
        token = UserToken.objects.create(user=user, token='123456')
        result = auth_service.logout(token)
        self.assertIsNone(result)
        self.assertEqual(len(UserToken.objects.filter(token='123456')), 0)


class AuthFunctionsAskUserEmailConfirmationTests(TestCase):
    def setUp(self):
        self.user = User.objects.create(email='aaa@aaa.aaa', first_name='222', surname='333')

    def test_user_already_verified(self):
        self.user.verified = True
        self.user.save()
        with self.assertRaises(CommonException) as error:
            auth_service.ask_user_email_confirmation(self.user)
        exception = error.exception
        self.assertEqual(str(exception), 'Электронная почта уже успешно подтверждена')
        self.assertEqual(exception.status, 400)

    def test_confirmation_created(self):
        auth_service.ask_user_email_confirmation(self.user)
        request = UserConfirmationRequest.objects.filter(user=self.user, type='email')
        self.assertEqual(len(request), 1)
        self.assertEqual(len(request[0].key), 10)


class AuthFunctionsConfirmUserEmailTests(TestCase):
    def setUp(self):
        self.user = User.objects.create(email='aaa@aaa.aaa', first_name='222', surname='333')
        self.request = UserConfirmationRequest.objects.create(user=self.user, key='55555', type='email')

    def test_user_already_verified(self):
        self.user.verified = True
        self.user.save()
        with self.assertRaises(CommonException) as error:
            auth_service.confirm_user_email(self.user, None)
        exception = error.exception
        self.assertEqual(str(exception), 'Электронная почта уже успешно подтверждена')
        self.assertEqual(exception.status, 400)

    def test_empty_verification_key(self):
        with self.assertRaises(CommonException) as error:
            auth_service.confirm_user_email(self.user, None)
        exception = error.exception
        self.assertEqual(str(exception), 'Не указан код')
        self.assertEqual(exception.status, 400)

    def test_incorrect_verification_key(self):
        with self.assertRaises(CommonException) as error:
            auth_service.confirm_user_email(self.user, '11111')
        exception = error.exception
        self.assertEqual(str(exception), 'Введён недествительный код')
        self.assertEqual(exception.status, 400)

    def test_old_verification_key(self):
        self.request.created -= timedelta(days=5)
        self.request.save()
        with self.assertRaises(CommonException) as error:
            auth_service.confirm_user_email(self.user, '55555')
        exception = error.exception
        self.assertEqual(str(exception),
                         'Данный код больше не действителен. Новый запрос отправлен на почту aaa@aaa.aaa.')
        self.assertEqual(exception.status, 400)

    def test_user_correctly_verified(self):
        auth_service.confirm_user_email(self.user, '55555')
        request = UserConfirmationRequest.objects.filter(user=self.user, type='email')
        self.assertEqual(len(request), 0)
        self.assertTrue(self.user.verified)
