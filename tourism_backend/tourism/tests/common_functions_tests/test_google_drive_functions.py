from ...common_functions import google_drive_functions
from ...exceptions.exceptions import CommonException
from ...google_drive import init_google_drive
from django.conf import settings
from django.test import TestCase

TEST_FILE_BASE_64 = 'iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAIAAACRXR' \
                    '/mAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUA' \
                    'AAAJcEhZcwAADsMAAA7DAcdvqGQAAAJMSURBVFhH7Z' \
                    'e9TgJBEMcpKSkpfQVLO+xMfAEKiytJbHgDCot7hCt5' \
                    'Aj8LKW1F/KgtjDY2NpSU6x92WCa4t19zmxDDLxeT25' \
                    'tjf8zMDWdL7SR7rRj+i9ZkMjlYMR6PaSkD0Vrdbre1' \
                    'ot1u01IGorW0ExgMBrSUgXQtOs9D9KejdlqrLEtayk' \
                    'C01nA41Fq71VuLxUJrgXwPo0ULI6DX6zm2NHXMlzCL' \
                    'lh4Bji3RVVoL0FLTWD6XNnRumbvxE7USGt/bGxzL3i' \
                    'GZSGh8b29wLFqBmTD2oTutoXMnlqDATPDGD0kYhSZr' \
                    'gcBMRCVMRwI6d2IPChwBPMz9qlNVFcVJtIDJhHsEmE' \
                    'YEuKXOzLwO9ft9WnJSq8Ub35EGNGJRFDoS1FWTLrda' \
                    'iKclJ7VauN8krG4zg6mm9SUME0tfBbTkwxXHW8ddSj' \
                    'emgoCWfHjieCnRtrQaA09V+AutR4uXEiT8Z5GQKuAP' \
                    '5aUEsIwyo9si3/2DvkHg4/aX2HFliIh2P25WYseVIe' \
                    '5LRMGbPXBcGTJqpTW7JpdW2lwwZNGCU6fTIan4VIHm' \
                    'tTA+uFNCqkAzWvP5fDQaYdiSy5o0J+DXQkX+7hdCsh' \
                    'PwayU4YUrFToQt/FpbPz6Gk8Ojh4uqPDvH3+L4NOHn' \
                    '0oGgt+5f1dWUjuuZuntWLx90SYxAa2kzVZePTG6qvn' \
                    '7oqgyBFiRIiJndzuiqDIHWzdPGhps1gUDr/Xujwo8m' \
                    '6ijQAltC+miijjItGCxVWAX1IUamVVdHMTItQGYsYW' \
                    '+fdEmAWCsPe61wlPoFIZID/vyTLCIAAAAASUVORK5CYII='


class GoogleDriveFunctionsUploadFileBytesTests(TestCase):
    def setUp(self):
        _bytes = []
        with open(settings.BASE_DIR / 'tourism/tests/test_image.png', 'rb') as f:
            _bytes = f.read()
        self.bytes = _bytes

    def test_works_correct(self):
        file_id = google_drive_functions.upload_file_bytes(len(self.bytes),
                                                           'image/png',
                                                           self.bytes,
                                                           init_google_drive.TEST_PHOTO_FOLDER_ID)
        self.assertGreater(len(file_id), 0)


class GoogleDriveFunctionsUploadFileBase64Tests(TestCase):
    def test_works_correct(self):
        file_id = google_drive_functions.upload_file_base64('image/png',
                                                            TEST_FILE_BASE_64,
                                                            init_google_drive.TEST_PHOTO_FOLDER_ID)
        self.assertGreater(len(file_id), 0)


class GoogleDriveFunctionsUploadFileTests(TestCase):
    def test_works_correct(self):
        gfile = google_drive_functions._upload_file_('test.png',
                                                     settings.BASE_DIR / 'tourism/tests/test_image.png',
                                                     'image/png',
                                                     init_google_drive.TEST_PHOTO_FOLDER_ID)
        self.assertIsNotNone(gfile)
        self.assertGreater(len(gfile['id']), 0)


class GoogleDriveFunctionsGenerateGoogleDriveFileNameTests(TestCase):
    def test_correct_file_name(self):
        file_name = google_drive_functions._generate_google_drive_file_name_('image/png')
        self.assertIsNotNone(file_name)
        self.assertTrue(file_name.endswith('.png'))
        self.assertGreater(len(file_name), 25)


class GoogleDriveFunctionsCheckFileSizeTests(TestCase):
    def test_big_file(self):
        with self.assertRaises(CommonException) as error:
            google_drive_functions._check_file_size_(100*1024*1024)
        exception = error.exception
        self.assertEqual(str(exception), 'Превышен допустимый объём загрузки файлов - 5 MB')
        self.assertEqual(exception.status, 400)

    def test_works_correct(self):
        result = google_drive_functions._check_file_size_(5*1024*1024)
        self.assertIsNone(result)


class GoogleDriveFunctionsCheckFileMimeTypeTests(TestCase):
    def test_incorrect_type(self):
        with self.assertRaises(CommonException) as error:
            google_drive_functions._check_file_mimetype_(None)
        exception = error.exception
        self.assertEqual(str(exception), 'Недопустимый формат изображениия')
        self.assertEqual(exception.status, 400)

    def test_png_correct(self):
        result = google_drive_functions._check_file_mimetype_('image/png')
        self.assertIsNone(result)

    def test_jpeg_correct(self):
        result = google_drive_functions._check_file_mimetype_('image/jpeg')
        self.assertIsNone(result)


class GoogleDriveFunctionsGetGoogleDriveFileAccessLinkTests(TestCase):
    def test_correct_link(self):
        link = google_drive_functions.get_google_drive_file_access_link('123456789')
        self.assertTrue(link.endswith('123456789'))
