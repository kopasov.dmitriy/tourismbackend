from ...common_functions import user_functions
from ...exceptions.exceptions import CommonException
from ...models import user, user_token, UserRoleClassifier
from datetime import date
from django.test import TestCase, RequestFactory


class UserFunctionsCheckUserRoleTests(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.role = UserRoleClassifier.objects.create(name='123', description='0000')
        self.user = User.objects.create(email='111', first_name='222', surname='333')
        UserToken.objects.create(user=self.user, token='12345')

    def _create_request_with_token_(self, token=None):
        request = self.factory.get('')
        if token is not None:
            request.headers = {'token': token}
        return request

    def test_empty_role(self):
        request = self._create_request_with_token_('12345')
        self.user.role = None
        self.user.save()
        with self.assertRaises(CommonException) as error:
            user_functions.check_user_role(request, '123')
        exception = error.exception
        self.assertEqual(str(exception), 'Для совершения данного действия пользователь должен обладать ролью 123')
        self.assertEqual(exception.status, 400)

    def test_incorrect_role(self):
        request = self._create_request_with_token_('12345')
        with self.assertRaises(CommonException) as error:
            user_functions.check_user_role(request, '55555')
        exception = error.exception
        self.assertEqual(str(exception), 'Для совершения данного действия пользователь должен обладать ролью 55555')
        self.assertEqual(exception.status, 400)

    def test_correct_role(self):
        self.user.role = self.role
        self.user.save()
        request = self._create_request_with_token_('12345')
        result = user_functions.check_user_role(request, '123')
        self.assertIsNone(result)


class UserFunctionsConvertDateTests(TestCase):
    def test_incorrect_date_format(self):
        with self.assertRaises(CommonException) as error:
            user_functions.convert_date('11111111')
        exception = error.exception
        self.assertEqual(str(exception), 'Неправильный формат даты. Нужный формат - "ДД.ММ.ГГГГ"')
        self.assertEqual(exception.status, 400)

    def test_correct_date(self):
        check_date = date(2020, 5, 22)
        _date = user_functions.convert_date('22.05.2020')
        self.assertEqual(check_date, _date)


class UserFunctionsValidateDateOfBirthdayTests(TestCase):
    def test_future_date(self):
        with self.assertRaises(CommonException) as error:
            user_functions.validate_date_of_birthday(date(2500, 1, 1))
        exception = error.exception
        self.assertEqual(str(exception), 'Указанная дата рождения больше текущей даты, что недопустимо')
        self.assertEqual(exception.status, 400)

    def test_too_early_date(self):
        with self.assertRaises(CommonException) as error:
            user_functions.validate_date_of_birthday(date(1000, 1, 1))
        exception = error.exception
        self.assertEqual(str(exception),
                         'Указана слишком ранняя дата рождения. Наличие таких старых людей весьма сомнительно')
        self.assertEqual(exception.status, 400)

    def test_works_correct(self):
        result = user_functions.validate_date_of_birthday(date(1997, 4, 23))
        self.assertIsNone(result)
