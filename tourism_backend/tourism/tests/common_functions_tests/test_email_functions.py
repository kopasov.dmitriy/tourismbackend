from ...common_functions import email_functions
from ...exceptions.exceptions import CommonException
from django.test import TestCase


class EmailFunctionsSendTextEmailTests(TestCase):
    def test_works_correct(self):
        result = email_functions.send_text_email('TEST', ('TEST '*5)[:-1], ['kopasov.dima@mail.ru'])
        self.assertIsNone(result)


class EmailFunctionsSendTextEmailAsHtmlTests(TestCase):
    def test_works_correct(self):
        result = email_functions.send_text_email_as_html('TEST', ('TEST '*5)[:-1], ['kopasov.dima@mail.ru'], 'test')
        self.assertIsNone(result)


class EmailFunctionsValidateEmailTests(TestCase):
    def test_empty_email(self):
        with self.assertRaises(CommonException) as error:
            email_functions.validate_email(None)
        exception = error.exception
        self.assertEqual(str(exception), 'Не указан email')
        self.assertEqual(exception.status, 400)

    def test_incorrect_email_format(self):
        with self.assertRaises(CommonException) as error:
            email_functions.validate_email('asdsadasdasasadsdasdsdsadasddasd')
        exception = error.exception
        self.assertEqual(str(exception), 'Указанный email имеет неправильный формат')
        self.assertEqual(exception.status, 400)

    def test_works_correct(self):
        result = email_functions.validate_email('kopasov.dima@mail.ru')
        self.assertIsNone(result)
