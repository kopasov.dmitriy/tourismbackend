from ...common_functions import auth_functions
from ...exceptions.exceptions import AuthException
from ...models import user, user_token, UserRoleClassifier
from django.test import TestCase, RequestFactory


class AuthFunctionsGetCurrentUserTokenTests(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        user = User.objects.create(email='111', first_name='222', surname='333')
        UserToken.objects.create(user=user, token='12345')

    def _create_request_with_token_(self, token=None):
        request = self.factory.get('')
        if token is not None:
            request.headers = {'token': token}
        return request

    def test_no_token(self):
        request = self._create_request_with_token_()
        with self.assertRaises(AuthException) as error:
            auth_functions.get_current_user_token(request)
        exception = error.exception
        self.assertEqual(str(exception), 'Пользователь не авторизован')
        self.assertEqual(exception.status, 401)

    def test_incorrect_token(self):
        request = self._create_request_with_token_('0000')
        with self.assertRaises(AuthException) as error:
            auth_functions.get_current_user_token(request)
        exception = error.exception
        self.assertEqual(str(exception), 'Пользователь не авторизован')
        self.assertEqual(exception.status, 401)

    def test_correct_user(self):
        request = self._create_request_with_token_('12345')
        user_token = auth_functions.get_current_user_token(request)
        self.assertEqual(user_token.token, '12345')
        self.assertEqual(user_token.user.email, '111')
        self.assertEqual(user_token.user.first_name, '222')
        self.assertEqual(user_token.user.surname, '333')


class AuthFunctionsGetCurrentUserTests(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        user = User.objects.create(email='111', first_name='222', surname='333')
        UserToken.objects.create(user=user, token='12345')

    def test_correct_user(self):
        request = self.factory.get('')
        request.headers = {'token': '12345'}
        user = auth_functions.get_current_user(request)
        self.assertEqual(user.email, '111')
        self.assertEqual(user.first_name, '222')
        self.assertEqual(user.surname, '333')


class AuthFunctionsGetUserByTokenTests(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        user = User.objects.create(email='111', first_name='222', surname='333')
        UserToken.objects.create(user=user, token='12345')

    def test_empty_user(self):
        user = auth_functions.get_user_by_token('0000')
        self.assertIsNone(user)

    def test_correct_user(self):
        user = auth_functions.get_user_by_token('12345')
        self.assertEqual(user.email, '111')
        self.assertEqual(user.first_name, '222')
        self.assertEqual(user.surname, '333')


class AuthFunctionsGenerateAuthToken(TestCase):
    def test_correct_token_with_different_length(self):
        token_1 = auth_functions.generate_auth_token(10)
        token_2 = auth_functions.generate_auth_token(20)
        token_3 = auth_functions.generate_auth_token(60)
        self.assertEqual(len(token_1), 10)
        self.assertNotEqual(token_1, auth_functions.generate_auth_token(10))
        self.assertEqual(len(token_2), 20)
        self.assertNotEqual(token_2, auth_functions.generate_auth_token(20))
        self.assertEqual(len(token_3), 60)
        self.assertNotEqual(token_3, auth_functions.generate_auth_token(60))


class AuthFunctionsHashPassword(TestCase):
    def test_correct_hash(self):
        password_hash = auth_functions.hash_password('123456789')
        self.assertEqual(len(password_hash), 32)
        self.assertEqual(password_hash, '25f9e794323b453885f5181f1b624d0b')


class AuthFunctionsFormAuthResponse(TestCase):
    def setUp(self):
        self.role = UserRoleClassifier.objects.create(name='123', description='0000')
        self.user = User.objects.create(email='111', first_name='222', surname='333')
        UserToken.objects.create(user=self.user, token='12345')

    def test_empty_role_and_not_verified(self):
        response = auth_functions.form_auth_response('12345', self.user)
        self.assertEqual(response['token'], '12345')
        self.assertIn('user_system_info', response)
        self.assertFalse(response['user_system_info']['role_selected'])
        self.assertFalse(response['user_system_info']['email_verified'])

    def test_takes_user_by_token(self):
        self.user.verified = True
        self.user.role = self.role
        self.user.save()
        response = auth_functions.form_auth_response('12345')
        self.assertEqual(response['token'], '12345')
        self.assertNotIn('user_system_info', response)
