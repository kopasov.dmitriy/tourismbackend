from django.contrib import admin
from django.urls import path, include
from .basic_api import check, restart_app, clear_logs, clear_test_data


urlpatterns = [
    path('tourism_api/', include('tourism.urls')),
    path('admin/', admin.site.urls),
    path('restart/', restart_app.restart_app),
    path('clear_logs/', clear_logs.clear_logs),
    path('clear_test_data/', clear_test_data.clear_test_data),
    path('', check.check),
]
