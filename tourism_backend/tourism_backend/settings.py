from corsheaders.defaults import default_headers
from pathlib import Path
import json
import os
#from pydrive.auth import GoogleAuth


def read_settings_parameters():
    with open(os.path.join(BASE_DIR / '../settings_parameters.json')) as settings_parameters_file:
        return json.load(settings_parameters_file)


BASE_DIR = Path(__file__).resolve().parent.parent

settings_parameters = read_settings_parameters()

SECRET_KEY = settings_parameters['SECRET_KEY']

#GoogleAuth.DEFAULT_SETTINGS['client_config_file'] = os.path.join(BASE_DIR / 'tourism/google_drive/client_secrets.json')

DEBUG = settings_parameters['DEBUG'] == 'True'

ALLOWED_HOSTS = [
    '127.0.0.1',
    'kopasov-development.ru',
    'www.kopasov-development.ru',
    'tourism.eu.pythonanywhere.com',
    'localhost'
]

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_USE_TLS = True
EMAIL_USE_SSL = False
EMAIL_PORT = 587
EMAIL_HOST_USER = 'tourism.project.test@gmail.com'
EMAIL_HOST_PASSWORD = settings_parameters['EMAIL_PASSWORD']

INSTALLED_APPS = [
    'tourism.apps.TourismConfig',
    'corsheaders',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'tourism_backend.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'tourism_backend.wsgi.application'

if DEBUG:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': BASE_DIR / 'db.sqlite3',
        }
    }
else:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'NAME': 'Tourism$default',
            'USER': 'Tourism',
            'PASSWORD': settings_parameters['MYSQL_PASSWORD'],
            'HOST': 'Tourism.mysql.eu.pythonanywhere-services.com',
        },
        'sqlite': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': BASE_DIR / 'db.sqlite3',
        }
    }


AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

CORS_ORIGIN_ALLOW_ALL = True

CORS_ALLOW_HEADERS = list(default_headers) + [
    'token',
]

STATIC_URL = '/static/'
STATIC_ROOT = 'static/'
