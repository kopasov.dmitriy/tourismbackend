import threading
from django.utils.deprecation import MiddlewareMixin
request_cfg = threading.local()

class RouterMiddleware(MiddlewareMixin):
    def __init__(self, get_response=None):
        super().__init__(get_response)

    def process_request(self, request):
        server = 'default'
        if 'use_test_server' in request.headers:
            server = 'sqlite'
        request_cfg.server = server

    def process_response(self, request, response):
        if hasattr(request_cfg, 'server'):
            del request_cfg.server
        return response


class DatabaseRouter:
    def get_db(self):
        if hasattr(request_cfg, 'server'):
            return request_cfg.server
        else:
            return 'default'

    def db_for_read(self, model, **hints):
        return self.get_db()

    def db_for_write(self, model, **hints):
        return self.get_db()