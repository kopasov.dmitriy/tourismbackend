from django.http import HttpResponse
from django.conf import settings


def restart_app(request):
    open(settings.BASE_DIR / '../.restart-app', 'w').close()
    return HttpResponse('OK')
