from django.http import HttpResponse


def clear_logs(request):
    open("../../../../../../var/log/tourism.eu.pythonanywhere.com.access.log", "w").close()
    open("../../../../../../var/log/tourism.eu.pythonanywhere.com.error.log", "w").close()
    open("../../../../../../var/log/tourism.eu.pythonanywhere.com.server.log", "w").close()
    return HttpResponse('cleared')
