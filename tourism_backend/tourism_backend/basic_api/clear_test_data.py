from django.http import JsonResponse
from tourism.models.user import User


def clear_test_data(request):
    users = User.objects.filter(email__istartswith='test')
    deleted_users = len(users)
    users.delete()
    return JsonResponse({
        'users': deleted_users,
    })
